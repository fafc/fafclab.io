package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
)

type MapCanvas struct {
	*Canvas
	backgroundLayer   *BackgroundLayer
	unitLayer         *UnitLayer
	selectedUnitLayer *SelectedUnitLayer
	shieldRangeLayer  *ShieldRangeLayer
	mouseLayer        *MouseLayer
	BoxColor          string
}

func NewMap(id string, onClick func(*Event), gridSize float64) *MapCanvas {
	cv := New(id, onClick, gridSize)
	width := cv.element.Width()
	height := cv.element.Height()
	mc := &MapCanvas{
		Canvas:            cv,
		backgroundLayer:   NewBackgroundLayer(width, height, gridSize),
		unitLayer:         NewUnitLayer(width, height, gridSize),
		selectedUnitLayer: NewSelectedUnitLayer(width, height, gridSize),
		shieldRangeLayer:  NewShieldRangeLayer(width, height, gridSize),
		mouseLayer:        NewMouseLayer(width, height, gridSize),
		BoxColor:          "black",
	}

	cv.addLayers(
		mc.backgroundLayer,
		mc.unitLayer,
		mc.selectedUnitLayer,
		mc.shieldRangeLayer,
		mc.mouseLayer,
	)

	// Custom event that indicates that all requested icons have been loaded and a redraw is required
	javascript.Window.AddEventListener("iconsLoaded", mc.iconsLoaded)

	return mc
}

func (mc *MapCanvas) drawUnits(units model.UnitSet) {
	mc.unitLayer.DrawUnits(units)
	mc.shieldRangeLayer.DrawUnits(units)
}

func (mc *MapCanvas) drawSelectedUnits(units model.UnitSet) {
	mc.selectedUnitLayer.DrawUnits(units)
}

func (mc *MapCanvas) drawMouse() {
	mc.mouseLayer.DrawBox(mc.mouseDownCoordinate, mc.mouseCoordinate, "box", mc.BoxColor)
}

func (mc *MapCanvas) clearAll() {
	mc.Canvas.ClearAll()
	mc.backgroundLayer.Draw(mc.topLeftCoordinate, mc.CoordinateAt(mc.Size()))
}

func (mc *MapCanvas) iconsLoaded(event javascript.Event) {
	mc.onClick(&Event{Update: true})
}

func (mc *MapCanvas) Redraw(m *model.Model, selected model.UnitSet) {
	mc.clearAll()
	mc.drawUnits(m.Units)
	mc.drawSelectedUnits(selected)
	mc.drawMouse()
	mc.drawParentLayer()
}
