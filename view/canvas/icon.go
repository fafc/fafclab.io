package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
)

var (
	icons           = make(map[string]*Icon)
	iconLoadedEvent = javascript.NewCustomEvent("iconsLoaded")
	redrawEventSet  = make(map[*Icon]struct{})
)

type Icon struct {
	*model.Icon
	javascript.Image
	loaded bool
}

func GetIcon(icon *model.Icon) *Icon {
	if i, ok := icons[icon.Name]; ok {
		return i
	}

	i := &Icon{
		Icon:  icon,
		Image: javascript.NewImage(),
	}
	i.SetSrc("res/icons/" + icon.Name + ".png")
	i.SetOnload(i.RegisterLoaded)
	icons[icon.Name] = i
	return i
}

func (i *Icon) RegisterLoaded(event javascript.Event) {
	i.loaded = true
	if _, ok := redrawEventSet[i]; ok {
		delete(redrawEventSet, i)
		if len(redrawEventSet) == 0 {
			javascript.Window.DispatchEvent(iconLoadedEvent)
		}
	}
}

func (i *Icon) Draw(f func(javascript.Image)) {
	if i.loaded {
		f(i.Image)
	} else {
		redrawEventSet[i] = struct{}{}
	}
}
