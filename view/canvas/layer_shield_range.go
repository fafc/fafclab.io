package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/util"

	"math"
)

type ShieldRangeLayer struct {
	BaseLayer
}

func NewShieldRangeLayer(width, height, gridSize float64) *ShieldRangeLayer {
	return &ShieldRangeLayer{BaseLayer: NewBaseLayer(width, height, gridSize)}
}

func (l *ShieldRangeLayer) DrawUnits(units model.UnitSet) {
	for unit := range units {
		if u, ok := unit.(model.ShieldGenerator); ok {
			l.ctx.BeginPath()
			l.DrawRange(unit.Center(), u.ShieldRange(), 2, u.Color(), true)
			l.ctx.ClosePath()
		}
	}

	oldGCO := l.ctx.GlobalCompositeOperation()
	l.ctx.SetGlobalCompositeOperation("destination-out")
	for unit := range units {
		if u, ok := unit.(model.ShieldGenerator); ok {
			l.ctx.BeginPath()
			l.DrawRange(unit.Center(), u.ShieldRange()-1, 2, "black", true)
			l.ctx.ClosePath()
		}
	}
	l.ctx.SetGlobalCompositeOperation(oldGCO)
}

func (l *ShieldRangeLayer) DrawRange(center util.Point, radius, thickness float64, color string, fill bool) {
	l.ctx.SetLineWidth(thickness)
	l.ctx.SetStrokeStyle(color)
	l.ctx.Arc(center.X*l.gridSize, center.Y*l.gridSize, radius*l.gridSize/4, 0, 2*math.Pi, false)

	if fill {
		l.ctx.SetFillStyle(color)
		l.ctx.Fill()
	}

	l.ctx.Stroke()
}
