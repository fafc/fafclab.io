package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/util"
)

type PlotLayer struct {
	BaseLayer
	color string
}

func NewPlotLayer(width, height, gridSize float64, color string) *PlotLayer {
	return &PlotLayer{
		BaseLayer: NewBaseLayer(width, height, gridSize),
		color:     color,
	}
}

func (l *PlotLayer) Draw(points []util.Point) {
	max := getMax(points)
	l.ctx.BeginPath()
	for _, point := range points {
		p := point.ScaleDown(max).Scale(util.Point{X: l.element.Width(), Y: l.element.Height()})
		l.ctx.LineTo(p.X, l.element.Height()-p.Y)
	}

	l.ctx.SetStrokeStyle(l.color)
	l.ctx.SetLineWidth(2)
	l.ctx.Stroke()
	l.ctx.ClosePath()
}

func getMax(points []util.Point) (pt util.Point) {
	for _, p := range points {
		if p.X > pt.X {
			pt.X = p.X
		}
		if p.Y > pt.Y {
			pt.Y = p.Y
		}
	}
	return
}
