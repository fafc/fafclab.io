package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/model"
)

type SelectedUnitLayer struct {
	*UnitLayer
	highlight string
}

func NewSelectedUnitLayer(width, height, gridSize float64) *SelectedUnitLayer {
	return &SelectedUnitLayer{
		highlight: "cyan",
		UnitLayer: NewUnitLayer(width, height, gridSize),
	}
}

func (l *SelectedUnitLayer) DrawUnits(units model.UnitSet) {
	for u := range units {
		l.drawUnit(u, 2, l.highlight)
	}
}
