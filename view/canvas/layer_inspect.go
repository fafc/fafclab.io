package canvas

import "gitlab.com/fafc/fafc.gitlab.io/util"

type InspectLayer struct {
	BaseLayer
}

func NewInspectLayer(width, height, gridSize float64) *InspectLayer {
	return &InspectLayer{BaseLayer: NewBaseLayer(width, height, gridSize)}
}

func (l *InspectLayer) Draw(position *util.Point) {
	l.ctx.BeginPath()
	l.ctx.MoveTo(position.X, 0)
	l.ctx.LineTo(position.X, l.element.Height())
	l.ctx.SetLineWidth(2)
	l.ctx.SetStrokeStyle("lightgray")
	l.ctx.Stroke()
	l.ctx.ClosePath()
}
