package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/util"
)

type MouseLayer struct {
	BaseLayer
}

func NewMouseLayer(width, height, gridSize float64) *MouseLayer {
	return &MouseLayer{BaseLayer: NewBaseLayer(width, height, gridSize)}
}

func (l *MouseLayer) DrawBox(topLeft, bottomRight *util.Point, shape, color string) {
	if topLeft == nil || bottomRight == nil || color == "" {
		return
	}

	c1 := topLeft.Multiply(l.gridSize)
	c2 := bottomRight.Multiply(l.gridSize)
	pt := c1.Min(c2)
	sz := c1.Subtract(c2).Abs()

	l.ctx.SetFillStyle(color)
	l.ctx.BeginPath()

	switch shape {
	case "box":
		l.ctx.Rect(pt.X, pt.Y, sz.X, sz.Y)
	case "line":
		l.ctx.MoveTo(pt.X, pt.Y)
		l.ctx.LineTo(pt.X+sz.X, pt.Y+sz.Y)
	}
	l.ctx.Stroke()
	l.ctx.ClosePath()
}
