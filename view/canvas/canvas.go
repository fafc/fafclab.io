package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/util"
	"math"
)

type Canvas struct {
	element             *javascript.Canvas
	ctx                 *javascript.Context
	parentLayer         *ParentLayer
	layers              []Layer
	scale               float64
	scaleMin            float64
	scaleMax            float64
	gridSize            float64
	onClick             func(*Event)
	controlDown         bool
	topLeftCoordinate   util.Point
	mouseDownCoordinate *util.Point
	mouseCoordinate     *util.Point
}

func New(id string, onClick func(*Event), gridSize float64) *Canvas {
	e := javascript.GetCanvas(id)
	c := &Canvas{
		element:     e,
		ctx:         e.GetContext("2d"),
		parentLayer: NewParentLayerOnCanvas(e, gridSize),
		layers:      make([]Layer, 0, 10),
		scale:       1,
		scaleMax:    5,
		scaleMin:    0.2,
		gridSize:    gridSize,
		onClick:     onClick,
	}

	c.element.AddEventListener("mousedown", c.mouseDown)
	c.element.AddEventListener("mouseup", c.mouseUp)
	c.element.AddEventListener("mousemove", c.mouseMove)
	c.element.AddEventListener("wheel", c.mouseScroll)
	c.element.AddEventListener("mouseleave", c.MouseLeave)

	return c
}

func (c *Canvas) addLayers(l ...Layer) {
	c.layers = append(c.layers, l...)
}

func (c *Canvas) mouseDown(event javascript.Event) {
	coordinate := c.CoordinateAt(util.Point{X: event.OffsetX(), Y: event.OffsetY()})
	c.mouseDownCoordinate = &coordinate
	e := c.mouseEvent(event)
	e.Down = true
	if e.Middle {
		e.PreventDefault()
	}
	c.onClick(e)
}

func (c *Canvas) mouseUp(event javascript.Event) {
	coordinate := c.CoordinateAt(util.Point{X: event.OffsetX(), Y: event.OffsetY()})
	c.mouseCoordinate = &coordinate

	e := c.mouseEvent(event)
	e.Up = true

	c.mouseDownCoordinate = nil
	c.onClick(e)
}

func (c *Canvas) MouseLeave(event javascript.Event) {
	c.mouseDownCoordinate = nil
	c.mouseCoordinate = nil
	e := NewEvent(event)
	e.Leave = true
	c.onClick(e)
}

func (c *Canvas) mouseMove(event javascript.Event) {
	e := c.mouseEvent(event)
	if e.Buttons()&4 != 0 {
		movement := util.Point{X: e.MovementX(), Y: e.MovementY()}.Divide(c.PixelsPerCoordinate())
		c.topLeftCoordinate.Subtract(movement)
		c.onClick(&Event{Update: true})
		return
	}
	e.Move = true
	c.onClick(e)
}

func (c *Canvas) mouseScroll(event javascript.Event) {
	var delta float64
	switch event.DeltaMode() {
	case javascript.DeltaPixel:
		delta = -float64(event.DeltaY()) / 40
	case javascript.DeltaLine:
		delta = -float64(event.DeltaY()) / 2
	default:
		delta = -float64(event.DeltaY())
	}

	if delta != 0 && event.Buttons()&4 == 0 {
		var scaleFactor float64
		if delta > 0 {
			scaleFactor = math.Pow(1.1, delta)
		} else {
			scaleFactor = math.Pow(1.0/1.1, -delta)
		}

		newScale := c.scale * scaleFactor
		if newScale > c.scaleMax {
			newScale = c.scaleMax
			scaleFactor = newScale / c.scale
		} else if newScale < c.scaleMin {
			newScale = c.scaleMin
			scaleFactor = newScale / c.scale
		}

		// When zooming, the coordinate under the mouse should stay the same after the zoom
		coordinateMousePointsTo := c.CoordinateAt(util.Point{X: event.OffsetX(), Y: event.OffsetY()})
		distanceToTopLeft := c.topLeftCoordinate.Subtract(coordinateMousePointsTo)
		diffDistanceToTopLeft := distanceToTopLeft.Multiply(1 - (1 / scaleFactor))

		// Calculate new center and zoom
		c.scale = newScale
		c.topLeftCoordinate = c.topLeftCoordinate.Subtract(diffDistanceToTopLeft)
		c.onClick(&Event{Update: true})
	}

	event.PreventDefault()
}

func (c *Canvas) mouseEvent(event javascript.Event) *Event {
	e := NewEvent(event)
	coordinate := c.CoordinateAt(util.Point{X: event.OffsetX(), Y: event.OffsetY()})
	c.mouseCoordinate = &coordinate
	e.Point1 = c.mouseDownCoordinate
	e.Point2 = c.mouseCoordinate

	buttons := event.Buttons()
	e.Left = buttons&1 != 0   // Left mouse
	e.Right = buttons&2 != 0  // Right Mouse
	e.Middle = buttons&3 != 0 // Middle mouse

	return e
}

func (c *Canvas) ResetView() {
	c.topLeftCoordinate = util.Point{X: 0, Y: 0}
	c.scale = 1
	c.onClick(&Event{Update: true})
}

func (c *Canvas) redraw() {
	c.ClearAll()
	c.drawParentLayer()
}

func (c *Canvas) drawParentLayer() {
	c.parentLayer.DrawLayers(c.layers)
	// Prepare the parent layer for any asynchronous draws
	c.parentLayer.Transform(c.scale, c.topLeftCoordinate)
}

func (c *Canvas) ClearAll() {
	c.parentLayer.Clear()
	for _, l := range c.layers {
		l.ClearAndTransform(c.scale, c.topLeftCoordinate)
	}
}

func (c *Canvas) CoordinateAt(pixel util.Point) util.Point {
	return pixel.Divide(c.PixelsPerCoordinate()).Add(c.topLeftCoordinate)
}

func (c *Canvas) PixelAt(coordinate util.Point) util.Point {
	return coordinate.Subtract(c.topLeftCoordinate).Multiply(c.PixelsPerCoordinate())
}

func (c *Canvas) PixelsPerCoordinate() float64 {
	return c.scale * c.gridSize
}

func (c *Canvas) Size() util.Point {
	return util.Point{
		X: c.element.Width(),
		Y: c.element.Height(),
	}
}

func (c *Canvas) SetOnClick(onClick func(*Event)) {
	c.onClick = onClick
}
