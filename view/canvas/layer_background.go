package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/util"
)

const (
	backgroundLight = "white"
	backgroundDark  = "lightgrey"
)

type BackgroundLayer struct {
	BaseLayer
}

func NewBackgroundLayer(width, height, gridSize float64) *BackgroundLayer {
	return &BackgroundLayer{BaseLayer: NewBaseLayer(width, height, gridSize)}
}

func NewBackgroundLayerOnCanvas(canvas *javascript.Canvas, gridSize float64) *BackgroundLayer {
	return &BackgroundLayer{
		BaseLayer{
			gridSize: gridSize,
			element:  canvas,
			ctx:      canvas.GetContext("2d"),
		}}
}

func (layer *BackgroundLayer) Draw(topLeft, bottomRight util.Point) {
	sizes := bottomRight.Subtract(topLeft).Multiply(layer.gridSize)
	topLeft = topLeft.Floor()
	bottomRight = bottomRight.Ceil()

	layer.ctx.BeginPath()
	layer.ctx.SetFillStyle(backgroundLight)
	layer.ctx.ClearRect(topLeft.X*layer.gridSize, topLeft.Y*layer.gridSize, sizes.X, sizes.Y)
	layer.ctx.SetFillStyle(backgroundDark)
	layer.tile(topLeft, bottomRight)
	layer.ctx.Fill()
	layer.ctx.ClosePath()
}

func (layer *BackgroundLayer) tile(topLeft, bottomRight util.Point) {
	for y := topLeft.Y; y < bottomRight.Y; y++ {
		xOffset := float64(int(topLeft.X+y) % 2)
		for x := topLeft.X + xOffset; x < bottomRight.X; x += 2 {
			layer.ctx.Rect(x*layer.gridSize, y*layer.gridSize, layer.gridSize, layer.gridSize)
		}
	}
}
