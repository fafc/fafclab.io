package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/util"
)

type UnitLayer struct {
	BaseLayer
	border string
}

func NewUnitLayer(width, height, gridSize float64) *UnitLayer {
	return &UnitLayer{
		border:    "black",
		BaseLayer: NewBaseLayer(width, height, gridSize),
	}
}

func (l *UnitLayer) DrawUnits(units model.UnitSet) {
	for u := range units {
		l.drawUnit(u, 1, l.border)
	}
}

func (l *UnitLayer) drawUnit(unit model.Unit, thickness float64, borderColor string) {
	loc := unit.Location().Multiply(l.gridSize)
	size := unit.Size().Multiply(l.gridSize)

	l.ctx.BeginPath()
	l.ctx.SetFillStyle(borderColor)
	l.ctx.FillRect(loc.X-thickness, loc.Y-thickness, size.X+thickness, size.Y+thickness)
	l.ctx.SetFillStyle(unit.Color())
	l.ctx.FillRect(loc.X, loc.Y, size.X-thickness, size.Y-thickness)

	if unit.Icon() != nil {
		GetIcon(unit.Icon()).Draw(func(i javascript.Image) {
			sz := util.Point{X: 12, Y: i.Height()}
			lc := loc.Add(size.Subtract(sz).Divide(2))
			l.ctx.DrawImage(i.Value, lc.X, lc.Y, sz.X, sz.Y)
		})
	}

	l.ctx.ClosePath()
}
