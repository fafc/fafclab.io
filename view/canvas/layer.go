package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/util"
)

type Layer interface {
	Element() *javascript.Canvas
	Clear()
	Transform(scale float64, topLeftCoordinate util.Point)
	ClearAndTransform(scale float64, topLeftCoordinate util.Point)
}

type BaseLayer struct {
	gridSize float64
	element  *javascript.Canvas
	ctx      *javascript.Context
}

func NewBaseLayer(width, height, gridSize float64) BaseLayer {
	element := javascript.NewCanvas()
	element.SetWidth(width)
	element.SetHeight(height)

	return BaseLayer{
		gridSize: gridSize,
		element:  element,
		ctx:      element.GetContext("2d"),
	}
}

func (layer *BaseLayer) Element() *javascript.Canvas {
	return layer.element
}

func (layer *BaseLayer) Clear() {
	layer.ctx.ResetTransform()
	// Clear entire screen, must be done before translation and scale
	layer.ctx.SetFillStyle("white")
	layer.ctx.ClearRect(0, 0, layer.element.Width(), layer.element.Height())
}

func (layer *BaseLayer) Transform(scale float64, topLeftCoordinate util.Point) {
	layer.ctx.ResetTransform()
	centerUnitPoint := topLeftCoordinate.Multiply(layer.gridSize)
	layer.ctx.Scale(scale, scale)
	layer.ctx.Translate(-centerUnitPoint.X, -centerUnitPoint.Y)
}

func (layer *BaseLayer) ClearAndTransform(scale float64, topLeftCoordinate util.Point) {
	layer.Clear()
	layer.Transform(scale, topLeftCoordinate)
}
