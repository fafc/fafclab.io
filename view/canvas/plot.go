package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/util"
)

type Plot struct {
	*Canvas
	plotLayers   map[string]*PlotLayer
	inspectLayer *InspectLayer
}

func NewPlot(id string, onClick func(*Event), gridSize float64) *Plot {
	p := &Plot{
		Canvas:     New(id, onClick, gridSize),
		plotLayers: make(map[string]*PlotLayer),
	}
	p.scale = 1
	p.scaleMax = 1
	p.scaleMin = 0.1
	p.inspectLayer = NewInspectLayer(p.element.Width(), p.element.Height(), 10)
	p.addLayers(p.inspectLayer)
	return p
}

func (p *Plot) AddGraph(name, color string) {
	l := NewPlotLayer(p.element.Width(), p.element.Height(), 10, color)
	p.addLayers(l)
	p.plotLayers[name] = l
}

func (p *Plot) drawGraphs(data model.EconomyData) {
	p.drawGraph("mass", data.Mass)
	p.drawGraph("energy", data.Energy)
	p.drawGraph("mass storage", data.MassStorage)
	p.drawGraph("energy storage", data.EnergyStorage)
}

func (p *Plot) drawGraph(key string, points []util.Point) {
	if l, ok := p.plotLayers[key]; ok {
		l.Draw(points)
	}
}

func (p *Plot) Redraw(model *model.Simulator, point *util.Point) {
	p.ClearAll()
	p.inspect(model, point)
	p.drawGraphs(model.Economy.Data)
	p.drawParentLayer()
}

func (p *Plot) inspect(model *model.Simulator, point *util.Point) {
	if point == nil {
		return
	}

	p.inspectLayer.Draw(point)
	if model == nil {
		return
	}

	dataLength := len(model.Economy.Data.Mass)
	if dataLength == 0 {
		return
	}

	plotLength := p.inspectLayer.Element().Width()
	dataPoint := int(point.X*float64(dataLength)/plotLength) - 1
	if dataPoint >= 0 && dataPoint <= dataLength {
		p.onClick(&Event{
			TableUpdate: true,
			DataPoint:   dataPoint,
		})
	}
}
