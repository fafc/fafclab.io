package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
)

type ParentLayer struct {
	BaseLayer
}

func NewParentLayer(width, height, gridSize float64) *ParentLayer {
	return &ParentLayer{BaseLayer: NewBaseLayer(width, height, gridSize)}
}

func NewParentLayerOnCanvas(canvas *javascript.Canvas, gridSize float64) *ParentLayer {
	return &ParentLayer{
		BaseLayer{
			gridSize: gridSize,
			element:  canvas,
			ctx:      canvas.GetContext("2d"),
		}}
}

func (l *ParentLayer) DrawLayers(layers []Layer) {
	l.ctx.ResetTransform()
	for _, layer := range layers {
		l.ctx.DrawImage(layer.Element().Value, 0, 0, layer.Element().Width(), layer.Element().Height())
	}
}
