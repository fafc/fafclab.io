package canvas

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/util"
)

type Event struct {
	javascript.Event
	Point1, Point2      *util.Point
	Left, Middle, Right bool
	Up, Down            bool
	Leave, Move         bool
	Update              bool
	Select, Remove      bool
	Unit                string

	// Simulation hooks
	Building    model.Unit
	Delete      model.Order
	TableUpdate bool
	DataPoint   int
	//Mass, Energy               float64
	//MassStorage, EnergyStorage float64
	//Time                       float64
}

func NewEvent(e javascript.Event) *Event {
	return &Event{Event: e}
}
