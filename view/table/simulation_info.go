package table

import (
	"fmt"
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
)

type SimulationInfo struct {
	time, mass, massStorage, energy, energyStorage javascript.Value
}

func NewSimulationInfo() *SimulationInfo {
	if javascript.Document.GetElementByID("sim-info").IsNull() {
		return nil
	}

	return &SimulationInfo{
		time:          javascript.Document.GetElementByID("sim-time"),
		mass:          javascript.Document.GetElementByID("sim-mass"),
		massStorage:   javascript.Document.GetElementByID("sim-mass-storage"),
		energy:        javascript.Document.GetElementByID("sim-energy"),
		energyStorage: javascript.Document.GetElementByID("sim-energy-storage"),
	}
}

func (t *SimulationInfo) Update(m *model.Simulator) {
	massStorage, energyStorage := m.Storage()

	t.time.SetInnerText(fmt.Sprintf("%.1f", m.Time))
	t.mass.SetInnerText(fmt.Sprintf("%.1f", m.Economy.Mass))
	t.massStorage.SetInnerText(fmt.Sprintf("%.1f", massStorage))
	t.energy.SetInnerText(fmt.Sprintf("%.1f", m.Economy.Energy))
	t.energyStorage.SetInnerText(fmt.Sprintf("%.1f", energyStorage))
}

func (t *SimulationInfo) InspectUpdate(time, mass, energy, massStorage, energyStorage float64) {
	t.time.SetInnerText(fmt.Sprintf("%.1f", time))
	t.mass.SetInnerText(fmt.Sprintf("%.1f", mass))
	t.massStorage.SetInnerText(fmt.Sprintf("%.1f", massStorage))
	t.energy.SetInnerText(fmt.Sprintf("%.1f", energy))
	t.energyStorage.SetInnerText(fmt.Sprintf("%.1f", energyStorage))
}
