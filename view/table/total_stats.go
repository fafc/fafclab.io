package table

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"

	"fmt"
)

type TotalStats struct {
	massCost, massYield, massROI, energyCost, energyYield, energyROI javascript.Value
}

func NewTotalStats() *TotalStats {
	if javascript.Document.GetElementByID("total-cost").IsNull() {
		return nil
	}

	return &TotalStats{
		massCost:    javascript.Document.GetElementByID("mass-cost"),
		massYield:   javascript.Document.GetElementByID("mass-yield"),
		massROI:     javascript.Document.GetElementByID("mass-roi"),
		energyCost:  javascript.Document.GetElementByID("energy-cost"),
		energyYield: javascript.Document.GetElementByID("energy-yield"),
		energyROI:   javascript.Document.GetElementByID("energy-roi"),
	}
}

func (t *TotalStats) Update(m *model.Model) {
	t.massCost.SetInnerText(fmt.Sprintf("%.1f", m.MassCost))
	t.massYield.SetInnerText(fmt.Sprintf("%.1f", m.MassYield))
	t.massROI.SetInnerText(fmt.Sprintf("%.1f", m.MassCost/m.MassYield))
	t.energyCost.SetInnerText(fmt.Sprintf("%.1f", m.EnergyCost))
	t.energyYield.SetInnerText(fmt.Sprintf("%.1f", m.EnergyYield))
	t.energyROI.SetInnerText(fmt.Sprintf("%.1f", m.EnergyCost/m.EnergyYield))
}
