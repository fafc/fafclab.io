package table

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"

	"fmt"
)

type UnitStats struct {
	unitName, massCost, massYield, energyCost, energyYield javascript.Value
}

func NewUnitStats() *UnitStats {
	if javascript.Document.GetElementByID("unit-info").IsNull() {
		return nil
	}

	return &UnitStats{
		unitName:    javascript.Document.GetElementByID("selected-unit"),
		massCost:    javascript.Document.GetElementByID("unit-mass-cost"),
		massYield:   javascript.Document.GetElementByID("unit-mass-yield"),
		energyCost:  javascript.Document.GetElementByID("unit-energy-cost"),
		energyYield: javascript.Document.GetElementByID("unit-energy-yield"),
	}
}

func (t *UnitStats) Update(units model.UnitSet) {
	name := "None"
	if len(units) > 1 {
		name = "Multiple units"
	} else if len(units) == 1 {
		for u := range units {
			name = u.Name()
		}
	}

	var massCost, massYield, energyCost, energyYield float64
	for unit := range units {
		if unit != nil {
			massCost += unit.MassCost()
			massYield += unit.NetMassYield()
			energyCost += unit.EnergyCost()
			energyYield += unit.NetEnergyYield()
		}
	}

	t.update(name, massCost, massYield, energyCost, energyYield)
}

func (t *UnitStats) update(name string, massCost, massYield, energyCost, energyYield float64) {
	t.unitName.SetInnerText(name)
	t.massCost.SetInnerText(fmt.Sprintf("%.1f", massCost))
	t.massYield.SetInnerText(fmt.Sprintf("%.1f", massYield))
	t.energyCost.SetInnerText(fmt.Sprintf("%.1f", energyCost))
	t.energyYield.SetInnerText(fmt.Sprintf("%.1f", energyYield))
}
