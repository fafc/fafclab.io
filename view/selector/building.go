package selector

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/view/canvas"
	"math"
)

type Building struct {
	*Selector
}

func NewBuildingSelector(callback func(*canvas.Event)) *Building {
	s := &Building{
		Selector: NewSelector("unit-selector", callback),
	}
	s.title = "Edit mode"
	s.setup(model.UnitDB)
	s.setSelect("select")
	return s
}

func (s *Building) setup(list map[string]model.UnitConstructor) {
	s.clearTable()
	s.addListenRow("select", "Select", s.selectSelect)
	for key, constructor := range list {
		unit := constructor(math.NaN(), math.NaN())
		unitID := key
		s.addListenRow(unitID, unit.Name(), func(javascript.Event) {
			s.selectUnit(unitID)
		})
	}
	s.addListenRow("remove", "Remove", s.selectRemove)
}

func (s *Building) selectSelect(event javascript.Event) {
	s.setSelect("select")
	s.callback(&canvas.Event{Select: true})
}

func (s *Building) selectRemove(event javascript.Event) {
	s.setSelect("remove")
	s.callback(&canvas.Event{Remove: true})
}

func (s *Building) selectUnit(unit string) {
	s.setSelect(unit)
	s.callback(&canvas.Event{Unit: unit})
}
