package selector

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/view/canvas"
)

type Selector struct {
	Selected string
	title    string
	element  javascript.Value
	callback func(*canvas.Event)
}

func NewSelector(id string, callback func(*canvas.Event)) *Selector {
	return &Selector{
		callback: callback,
		element:  javascript.Document.GetElementByID(id),
	}
}

func (s *Selector) colorItem(id string, selected bool) {
	item := javascript.Document.GetElementByID("selector-" + id)
	if item.IsNull() {
		return
	}

	if selected {
		item.ClassList().Add("selected")
	} else {
		item.ClassList().Remove("selected")
	}
}

func (s *Selector) setSelect(item string) {
	s.colorItem(s.Selected, false)
	s.Selected = item
	s.colorItem(s.Selected, true)
}

func (s *Selector) clearTable() {
	s.element.SetInnerHTML("")
	row := s.element.InsertRow()
	row.SetInnerHTML("<th>" + s.title + "</th><th></th>")
}

func (s *Selector) addListenRow(id, content string, f func(javascript.Event)) {
	row := s.element.InsertRow()
	row.SetID("selector-" + id)
	row.InsertCell().SetInnerText(content)
	row.InsertCell() // dummy cell for future use

	javascript.Document.GetElementByID("selector-"+id).AddEventListener("click", f)
}
