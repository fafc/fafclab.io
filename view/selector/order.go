package selector

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/view/canvas"
)

type Order struct {
	*Selector
}

func NewOrderSelector(callback func(*canvas.Event)) *Order {
	s := &Order{
		Selector: NewSelector("order-table", callback),
	}
	s.title = "Build Order"
	s.Setup(model.NewOrderQueue())
	return s
}

func (s *Order) Setup(orders *model.OrderQueue) {
	s.clearTable()
	for _, o := range orders.Queue {
		order := o
		s.addListenRow(order.ID(), order.Description(), "\u274C",
			func(javascript.Event) { s.selectOrder(order) },
			func(javascript.Event) { s.deleteOrder(order) },
		)
	}
}

func (s *Order) addListenRow(id, content, icon string, f1, f2 func(javascript.Event)) {
	row := s.element.InsertRow()
	row.SetID("selector-" + id)
	c1 := row.InsertCell()
	c1.SetInnerText(content)
	c1.AddEventListener("click", f1)
	c2 := row.InsertCell()
	c2.SetInnerText(icon)
	c2.AddEventListener("click", f2)
}

func (s *Order) deleteOrder(order model.Order) {
	s.callback(&canvas.Event{Delete: order})
}

func (s *Order) selectOrder(order model.Order) {
	s.setSelect(order.ID())
}
