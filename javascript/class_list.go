package javascript

type ClassList struct {
	Value
}

func (l ClassList) Add(v string) {
	l.Call("add", v)
}

func (l ClassList) Remove(v string) {
	l.Call("remove", v)
}
