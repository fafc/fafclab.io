package javascript

var Document = document{Window.Get("document")}

type document struct {
	Value
}

func (d *document) GetElementByID(id string) Value {
	return d.Call("getElementById", id)
}

func (d *document) CreateElement(tag string) Value {
	return d.Call("createElement", tag)
}
