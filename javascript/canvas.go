package javascript

type Canvas struct {
	Value
}

func NewCanvas() *Canvas {
	return &Canvas{Document.CreateElement("canvas")}
}

func GetCanvas(id string) *Canvas {
	return &Canvas{
		Value: Document.GetElementByID(id),
	}
}

func (c *Canvas) GetContext(ctxType string) *Context {
	return &Context{c.Call("getContext", ctxType)}
}

func (c *Canvas) Width() float64 {
	return c.Get("width").Float()
}

func (c *Canvas) Height() float64 {
	return c.Get("height").Float()
}

func (c *Canvas) SetWidth(w float64) {
	c.Set("width", w)
}

func (c *Canvas) SetHeight(h float64) {
	c.Set("height", h)
}
