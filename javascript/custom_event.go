package javascript

import "syscall/js"

type CustomEvent struct {
	Value
}

func NewCustomEvent(args ...interface{}) CustomEvent {
	return CustomEvent{Value{js.Global().Get("CustomEvent").New(args)}}
}
