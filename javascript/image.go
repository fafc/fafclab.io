package javascript

import (
	"syscall/js"
)

type Image struct {
	Value
}

func NewImage(args ...interface{}) Image {
	return Image{Value{js.Global().Get("Image").New(args)}}
}

func (i *Image) Width() float64 {
	return i.Get("width").Float()
}

func (i *Image) Height() float64 {
	return i.Get("height").Float()
}

func (i *Image) Src() string {
	return i.Get("src").String()
}

func (i *Image) SetSrc(src string) {
	i.Set("src", src)
}
