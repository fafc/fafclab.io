package javascript

import "syscall/js"

type Value struct {
	js.Value
}

func ValueOf(x interface{}) Value {
	return Value{js.ValueOf(x)}
}

func (v Value) Call(m string, args ...interface{}) Value {
	return Value{v.Value.Call(m, args...)}
}

func (v Value) Get(p string) Value {
	return Value{v.Value.Get(p)}
}

func (v Value) Index(i int) Value {
	return Value{v.Value.Index(i)}
}

func (v Value) Invoke(args ...interface{}) Value {
	return Value{v.Value.Invoke(args...)}
}

func (v Value) JSValue() Value {
	return Value{v.Value.JSValue()}
}

func (v Value) New(args ...interface{}) Value {
	return Value{v.Value.New(args...)}
}

func (v Value) Constructor() Value {
	return v.Get("constructor")
}

func (v Value) Name() string {
	return v.Get("Name").String()
}

func (v Value) AddEventListener(event string, fn func(event Event)) {
	v.Call("addEventListener", event,
		js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			if len(args) >= 1 {
				fn(Event{Value{args[0]}})
			} else {
				fn(Event{Value{js.Null()}})
			}
			return nil
		}),
	)
}

func (v Value) ClassList() ClassList {
	return ClassList{v.Get("classList")}
}

func (v Value) SetInnerHTML(s string) {
	v.Set("innerHTML", s)
}

func (v Value) SetInnerText(s string) {
	v.Set("innerText", s)
}

func (v Value) InsertRow() Value {
	return v.Call("insertRow")
}

func (v Value) InsertCell() Value {
	return v.Call("insertCell")
}

func (v Value) ID() string {
	return v.Get("id").String()
}

func (v Value) SetID(id string) {
	v.Set("id", id)
}

func (v Value) SetOnclick(fn func(event Event)) {
	v.Set("onclick", js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		if len(args) >= 1 {
			fn(Event{Value{args[0]}})
		} else {
			fn(Event{Value{js.Null()}})
		}
		return nil
	}))
}

func (v Value) SetOnload(fn func(event Event)) {
	v.Set("onload", js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		if len(args) >= 1 {
			fn(Event{Value{args[0]}})
		} else {
			fn(Event{Value{js.Null()}})
		}
		return nil
	}))
}

func (v Value) DispatchEvent(event CustomEvent) {
	v.Call("dispatchEvent", event.Value.Value)
}
