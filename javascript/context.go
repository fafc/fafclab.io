package javascript

type Context struct {
	Value
}

func (ctx *Context) ResetTransform() {
	ctx.Call("resetTransform")
}

func (ctx *Context) SetFillStyle(color string) {
	ctx.Set("fillStyle", color)
}

func (ctx *Context) ClearRect(x, y, w, h float64) {
	ctx.Call("clearRect", x, y, w, h)
}

func (ctx *Context) Scale(x, y float64) {
	ctx.Call("scale", x, y)
}

func (ctx *Context) Translate(x, y float64) {
	ctx.Call("translate", x, y)
}

func (ctx *Context) BeginPath() {
	ctx.Call("beginPath")
}

func (ctx *Context) Rect(x, y, w, h float64) {
	ctx.Call("rect", x, y, w, h)
}

func (ctx *Context) Fill() {
	ctx.Call("fill")
}

func (ctx *Context) ClosePath() {
	ctx.Call("closePath")
}

func (ctx *Context) MoveTo(x, y float64) {
	ctx.Call("moveTo", x, y)
}

func (ctx *Context) LineTo(x, y float64) {
	ctx.Call("lineTo", x, y)
}

func (ctx *Context) Stroke() {
	ctx.Call("stroke")
}

func (ctx *Context) DrawImage(v Value, x, y, w, h float64) {
	ctx.Call("drawImage", v.Value, x, y, w, h)
}

func (ctx *Context) FillRect(x, y, w, h float64) {
	ctx.Call("fillRect", x, y, w, h)
}

func (ctx *Context) GlobalCompositeOperation() string {
	return ctx.Get("globalCompositeOperation").String()
}

func (ctx *Context) SetGlobalCompositeOperation(s string) {
	ctx.Set("globalCompositeOperation", s)
}

func (ctx *Context) LineWidth() float64 {
	return ctx.Get("lineWidth").Float()
}

func (ctx *Context) SetLineWidth(v float64) {
	ctx.Set("lineWidth", v)
}

func (ctx *Context) StrokeStyle() string {
	return ctx.Get("strokeStyle").String()
}

func (ctx *Context) SetStrokeStyle(s string) {
	ctx.Set("strokeStyle", s)
}

func (ctx *Context) Arc(x, y, radius, startAngle, endAngle float64, anticlockwise bool) {
	ctx.Call("arc", x, y, radius, startAngle, endAngle, anticlockwise)
}
