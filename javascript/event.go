package javascript

// Delta modes for a WheelEvent.
// See: https://developer.mozilla.org/en-US/docs/Web/API/WheelEvent
const (
	DeltaPixel = 0x00
	DeltaLine  = 0x01
	DeltaPage  = 0x02
)

type Event struct {
	Value
}

func (e *Event) OffsetX() float64 {
	return e.Get("offsetX").Float()
}

func (e *Event) OffsetY() float64 {
	return e.Get("offsetY").Float()
}

func (e *Event) MovementX() float64 {
	return e.Get("movementX").Float()
}

func (e *Event) MovementY() float64 {
	return e.Get("movementY").Float()
}

func (e *Event) Button() int {
	return e.Get("buttons").Int()
}

func (e *Event) Buttons() uint {
	return uint(e.Get("buttons").Int())
}

func (e *Event) PreventDefault() {
	e.Call("preventDefault")
}

func (e *Event) WheelDelta() int {
	return e.Get("wheelDelta").Int()
}

func (e *Event) DeltaY() int {
	return e.Get("deltaY").Int()
}

func (e *Event) DeltaMode() int {
	return e.Get("deltaMode").Int()
}

func (e *Event) Detail() int {
	return e.Get("detail").Int()
}

func (e *Event) Key() string {
	return e.Get("detail").String()
}

func (e *Event) CtrlKey() bool {
	return e.Get("ctrlKey").Bool()
}

func (e *Event) Target() Value {
	return e.Get("target")
}
