package main

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/view_model"
)

var (
	viewModel view_model.ViewModel
)

func main() {
	// Register preset callbacks
	javascript.Document.GetElementByID("preset-reset").SetOnclick(func(javascript.Event) { resetView() })
	javascript.Document.GetElementByID("preset-preset0").SetOnclick(func(javascript.Event) { loadPreset(0) })
	javascript.Document.GetElementByID("preset-preset1").SetOnclick(func(javascript.Event) { loadPreset(1) })
	javascript.Document.GetElementByID("preset-preset2").SetOnclick(func(javascript.Event) { loadPreset(2) })
	javascript.Document.GetElementByID("preset-preset3").SetOnclick(func(javascript.Event) { loadPreset(3) })

	viewModel = view_model.NewViewModel(model.NewModel())
	viewModel.LoadLocation()
	select {}
}

func loadPreset(nr int) {
	viewModel.Load(presets[nr])
}

func resetView() {
	viewModel.ResetView()
}

var presets = []map[string]string{
	// Empty
	{},
	// T3 mass extractor
	{"15,14": "t3mx", "16,14": "ms", "15,13": "ms", "14,14": "ms", "15,15": "ms"},
	// T2 Mass Fabricator flower
	{
		"2,1": "t1pg",
		"1,2": "t1pg",
		"2,2": "t2mf",
		"3,2": "t1pg",
		"2,3": "t1pg",
		"2,0": "es",
		"3,1": "es",
		"4,2": "es",
		"3,3": "es",
		"2,4": "es",
		"1,3": "es",
		"0,2": "es",
		"1,1": "es",
	},
	// T3 Fabricator (row-scalable)
	{
		"10,10": "t3pg",
		"10,7":  "t3mf",
		"11,14": "t3mf",
		"13,7":  "ms",
		"13,8":  "ms",
		"13,9":  "ms",
		"10,14": "ms",
		"10,15": "ms",
		"10,16": "ms",
		"10,6":  "ms",
		"11,6":  "ms",
		"12,6":  "ms",
		"11,17": "ms",
		"12,17": "ms",
		"13,17": "ms",
	},
}
