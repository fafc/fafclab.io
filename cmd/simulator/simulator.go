package main

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/view_model"
)

var (
	sim view_model.ViewModelSimulator
)

func main() {
	// Register preset callbacks
	javascript.Document.GetElementByID("preset-reset").SetOnclick(func(javascript.Event) { resetView() })
	javascript.Document.GetElementByID("preset-clear").SetOnclick(func(javascript.Event) { clear() })
	javascript.Document.GetElementByID("preset-start").SetOnclick(func(javascript.Event) { start() })
	javascript.Document.GetElementByID("preset-tick60").SetOnclick(func(javascript.Event) { tick(60) })

	sim = view_model.NewSimulator(model.NewSimulator())
	sim.LoadLocation()
	sim.Update()
	select {}
}

func clear() {
	sim.Load(make(map[string]string))
}

func start() {
	sim.Clear()
	sim.Update()
}

func tick(count int) {
	for i := 0; i < count; i++ {
		sim.Tick()
	}
	sim.Update()
}

func resetView() {
	sim.ResetView()
}
