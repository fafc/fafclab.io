package view_model

import (
	"fmt"
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/util"
	"gitlab.com/fafc/fafc.gitlab.io/view/canvas"
	"gitlab.com/fafc/fafc.gitlab.io/view/selector"
	"gitlab.com/fafc/fafc.gitlab.io/view/table"
)

type ViewModelSimulator interface {
	ViewModel
	Clear()
	Tick()
}

type Simulator struct {
	*PowerFlower
	sim            *model.Simulator
	plot           *canvas.Plot
	simulationInfo *table.SimulationInfo
	orderSelector  *selector.Order
}

func NewSimulator(sim *model.Simulator) ViewModelSimulator {
	s := &Simulator{
		PowerFlower:    NewViewModel(sim.Model).(*PowerFlower),
		sim:            sim,
		simulationInfo: table.NewSimulationInfo(),
	}
	s.PowerFlower.canvas.SetOnClick(s.CanvasEvent)
	s.plot = canvas.NewPlot("plot", s.plotClick, 16)
	s.plot.AddGraph("energy", "goldenrod")
	s.plot.AddGraph("mass", "limegreen")
	s.plot.AddGraph("mass storage", "blue")
	s.plot.AddGraph("energy storage", "yellow")
	s.orderSelector = selector.NewOrderSelector(s.orderSelectorClick)
	return s
}

func (s *Simulator) Update() {
	s.unitStatsTable.Update(s.selected)
	s.simulationInfo.Update(s.sim)
	if s.selectedBuilder != nil {
		s.orderSelector.Setup(s.selectedBuilder.Orders())
	} else {
		s.orderSelector.Setup(model.NewOrderQueue())
	}

	s.canvas.Redraw(s.model, s.selected)
	if s.plot != nil {
		s.plot.Redraw(s.sim, nil)
	}
	s.location.Set(s.model.Export())
}

func (s *Simulator) plotClick(event *canvas.Event) {
	if event.TableUpdate {
		time := s.sim.Economy.Data.Mass[event.DataPoint].X
		mass := s.sim.Economy.Data.Mass[event.DataPoint].Y
		energy := s.sim.Economy.Data.Energy[event.DataPoint].Y
		massStorage := s.sim.Economy.Data.MassStorage[event.DataPoint].Y
		energyStorage := s.sim.Economy.Data.EnergyStorage[event.DataPoint].Y
		s.simulationInfo.InspectUpdate(time, mass, energy, massStorage, energyStorage)
		return
	}

	if event.Point2 != nil {
		pt := s.plot.PixelAt(*event.Point2)
		s.plot.Redraw(s.sim, &pt)
		return
	}

	s.Update()
}

func (s *Simulator) orderSelectorClick(event *canvas.Event) {
	if event.Building != nil {
		s.clearSelection()
		s.selected.Add(event.Building)
	}

	if event.Delete != nil {
		s.selectedBuilder.RemoveOrder(event.Delete)
		s.clearSelection()
		s.sim.Run()
	}

	s.Update()
}

func (s *Simulator) resetView() {
	s.PowerFlower.ResetView()
	s.plot.ResetView()
}

func (s *Simulator) AddUnit(name string, placePoint util.Point) error {
	if s.selectedBuilder == nil {
		return fmt.Errorf("no selected builder")
	}

	f, ok := model.UnitDB[name]
	if !ok {
		return fmt.Errorf("unknown unit name %q", name)
	}

	unit := f(placePoint.X, placePoint.Y)
	if len(s.model.FindCollisions(unit)) == 0 {
		s.selectedBuilder.AddOrder(model.NewBuildOrder(unit))
		s.sim.Run()
	}

	return nil
}

func (s *Simulator) Clear() {
	s.sim.ClearSimulation()
	s.clearSelection()
	s.selectedBuilder = s.sim.ACU
	s.selected.Add(s.sim.ACU)
}

func (s *Simulator) Tick() {
	s.sim.Tick()
}

func (s *Simulator) CanvasEvent(event *canvas.Event) {
	if event.Up && s.SelectionMode() {
		s.SelectUnits(event.Point1, event.Point2, event.CtrlKey())
		if s.selector.Selected == "remove" {
			s.deleteSelection(s.selected)
		}
	}

	if event.Left && event.Down && !s.SelectionMode() {
		placePoint := event.Point1.Floor()
		s.AddUnit(s.selector.Selected, placePoint)
	}

	s.Update()
}
