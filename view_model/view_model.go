package view_model

import (
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
	"gitlab.com/fafc/fafc.gitlab.io/model"
	"gitlab.com/fafc/fafc.gitlab.io/util"
	"gitlab.com/fafc/fafc.gitlab.io/view/canvas"
	"gitlab.com/fafc/fafc.gitlab.io/view/selector"
	"gitlab.com/fafc/fafc.gitlab.io/view/table"
	"math"
)

const (
	canvasID = "grid"
	gridSize = 16
)

type ViewModel interface {
	ResetView()
	Update()
	SelectionMode() bool
	SelectorUpdate(event *canvas.Event)
	SelectUnits(pt1, pt2 *util.Point, controlDown bool)
	CanvasEvent(event *canvas.Event)
	LoadLocation()
	Load(data map[string]string)
	AddUnit(name string, placePoint util.Point) error
}

type PowerFlower struct {
	unitStatsTable  *table.UnitStats
	totalStatsTable *table.TotalStats
	canvas          *canvas.MapCanvas
	model           *model.Model
	selector        *selector.Building
	location        *Location
	selected        model.UnitSet
	selectedBuilder model.Builder
	keyDownMap      map[string]func(javascript.Event)
	keyUpMap        map[string]func(javascript.Event)
}

func NewViewModel(mdl *model.Model) ViewModel {
	vm := &PowerFlower{
		unitStatsTable:  table.NewUnitStats(),
		totalStatsTable: table.NewTotalStats(),
		model:           mdl,
		location:        NewLocation(),
		selected:        make(model.UnitSet),
		keyDownMap:      make(map[string]func(javascript.Event)),
		keyUpMap:        make(map[string]func(javascript.Event)),
	}

	vm.selector = selector.NewBuildingSelector(vm.SelectorUpdate)
	vm.canvas = canvas.NewMap(canvasID, vm.CanvasEvent, gridSize)
	vm.registerKeyDown("Delete", vm.deleteKeyEvent)

	javascript.Document.AddEventListener("keydown", vm.keyDown)
	javascript.Document.AddEventListener("keyup", vm.keyUp)
	javascript.Document.AddEventListener("focusout", vm.focusOut)
	javascript.Document.AddEventListener("updateViewModel", vm.updateEvent)

	return vm
}

func (vm *PowerFlower) ResetView() {
	vm.canvas.ResetView()
}

func (vm *PowerFlower) registerKeyDown(key string, fn func(javascript.Event)) {
	vm.keyDownMap[key] = fn
}

func (vm *PowerFlower) registerKeyUp(key string, fn func(javascript.Event)) {
	vm.keyDownMap[key] = fn
}

func (vm *PowerFlower) keyDown(event javascript.Event) {
	f, ok := vm.keyDownMap[event.Key()]
	if ok {
		f(event)
	}
}

func (vm *PowerFlower) keyUp(event javascript.Event) {
	f, ok := vm.keyUpMap[event.Key()]
	if ok {
		f(event)
	}
}

func (vm *PowerFlower) focusOut(event javascript.Event) {
	if event.Target().ID() == "grid" {
		vm.canvas.MouseLeave(event)
	}
}

func (vm *PowerFlower) updateEvent(event javascript.Event) {
	vm.Update()
}

func (vm *PowerFlower) deleteKeyEvent(event javascript.Event) {
	vm.deleteSelection(vm.selected)
}

func (vm *PowerFlower) Update() {
	if vm.unitStatsTable != nil {
		vm.unitStatsTable.Update(vm.selected)
	}
	if vm.totalStatsTable != nil {
		vm.totalStatsTable.Update(vm.model)
	}
	vm.canvas.Redraw(vm.model, vm.selected)
	vm.location.Set(vm.model.Export())
}

func (vm *PowerFlower) clearSelection() {
	vm.selected = make(model.UnitSet)
	vm.Update()
}

func (vm *PowerFlower) deleteSelection(selection model.UnitSet) {
	vm.model.DeleteUnitSet(selection)
	vm.clearSelection()
}

func (vm *PowerFlower) SelectionMode() bool {
	return vm.selector.Selected == "select" || vm.selector.Selected == "remove"
}

func (vm *PowerFlower) SelectorUpdate(event *canvas.Event) {
	vm.clearSelection()
	switch {
	case event.Select, event.Remove:
		vm.canvas.BoxColor = "black"
	case event.Unit != "":
		vm.selected.Add(model.UnitDB[event.Unit](math.NaN(), math.NaN()))
		vm.canvas.BoxColor = ""
	}
}

func (vm *PowerFlower) SelectUnits(pt1, pt2 *util.Point, controlDown bool) {
	unitsInBox := vm.model.FindBetweenCoordinates(pt1, pt2)

	if !controlDown {
		vm.selected = unitsInBox
		return
	}

	for unit := range unitsInBox {
		if _, ok := vm.selected[unit]; ok {
			vm.selected.Delete(unit)
		} else {
			vm.selected.Add(unit)
		}
	}

	if len(vm.selected) > 0 {
		for unit := range vm.selected {
			if b, ok := unit.(model.Builder); ok {
				vm.selectedBuilder = b
			}
			break
		}
	}
}

func (vm *PowerFlower) CanvasEvent(event *canvas.Event) {
	if event.Up && vm.SelectionMode() {
		vm.SelectUnits(event.Point1, event.Point2, event.CtrlKey())
		if vm.selector.Selected == "remove" {
			vm.deleteSelection(vm.selected)
		}
	}

	if event.Left && event.Down && !vm.SelectionMode() {
		placePoint := event.Point1.Floor()
		vm.AddUnit(vm.selector.Selected, placePoint)
	}

	vm.Update()
}

func (vm *PowerFlower) LoadLocation() {
	data, err := vm.location.Get()
	if err != nil {
		return
	}
	vm.Load(data)
}

func (vm *PowerFlower) Load(data map[string]string) {
	vm.ResetView()
	vm.clearSelection()
	vm.model.Load(data)
	vm.Update()
}
func (vm *PowerFlower) AddUnit(name string, placePoint util.Point) error {
	return vm.model.Add(model.UnitDB[name](placePoint.X, placePoint.Y))
}
