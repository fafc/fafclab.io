package view_model

import (
	"encoding/base64"
	"encoding/json"
	"gitlab.com/fafc/fafc.gitlab.io/javascript"
)

type Location struct {
	location javascript.Value
}

func NewLocation() *Location {
	return &Location{location: javascript.Window.Get("location")}
}

func (l *Location) Get() (map[string]string, error) {
	hash := l.location.Get("hash").String()
	if hash == "" {
		return nil, nil
	}

	i := make(map[string]string)
	data, err := base64.URLEncoding.DecodeString(hash[1:])
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &i)
	if err != nil {
		return nil, err
	}

	return i, nil
}

func (l *Location) Set(i map[string]string) error {
	data, err := json.Marshal(i)
	if err != nil {
		return err
	}
	l.location.Set("hash", "#"+base64.URLEncoding.EncodeToString(data))
	return nil
}
