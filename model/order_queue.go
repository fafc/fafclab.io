package model

type OrderQueue struct {
	Queue   []Order
	counter int
}

func NewOrderQueue() *OrderQueue {
	return new(OrderQueue)
}

func (q *OrderQueue) HasOrder() bool {
	return q.counter < len(q.Queue)
}

func (q *OrderQueue) Add(order Order) {
	q.Queue = append(q.Queue, order)
}

func (q *OrderQueue) Delete(order Order) {
	for i, o := range q.Queue {
		if o == order {
			q.Queue = append(q.Queue[:i], q.Queue[i+1:]...)
		}
	}
}

func (q *OrderQueue) Clear() {
	q.counter = 0
	for _, o := range q.Queue {
		o.Reset()
	}
}

func (q *OrderQueue) Update(unit Unit, ratio float64) (mass, energy float64) {
	if q.HasOrder() {
		return q.Queue[q.counter].Update(unit, ratio)
	}
	return 0, 0
}

func (q *OrderQueue) CurrentOrder() Order {
	if !q.HasOrder() {
		return nil
	}
	return q.Queue[q.counter]
}

func (q *OrderQueue) RequestedResources(unit Unit) (mass, energy float64) {
	q.updateQueue()
	if !q.HasOrder() {
		return 0, 0
	}
	return q.Queue[q.counter].RequestedResources(unit)
}

func (q *OrderQueue) updateQueue() {
	if q.HasOrder() && q.Queue[q.counter].Finished() {
		q.counter++
	}
}

func (q *OrderQueue) FetchCompletedUnit() Unit {
	if b, ok := q.CurrentOrder().(*BuildOrder); ok {
		return b.FetchCompleted()
	}
	return nil
}
