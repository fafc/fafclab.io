package model

import "fmt"

type UnitSet map[Unit]struct{}

func (set UnitSet) Add(u Unit) {
	set[u] = struct{}{}
}

func (set UnitSet) Delete(u Unit) {
	delete(set, u)
}

// String returns a human readable newline separated list with the content of the UnitSet.
func (set UnitSet) String() (s string) {
	for unit := range set {
		s += fmt.Sprintf("%s: %s\n", unit.Name(), unit.Location())
	}
	return
}
