package model

import (
	"gitlab.com/fafc/fafc.gitlab.io/util"
	"math"
)

type EconomyData struct {
	Mass          []util.Point
	Energy        []util.Point
	MassStorage   []util.Point
	EnergyStorage []util.Point
}

type Economy struct {
	Mass, massRequest, combinedMassRequest           float64
	Energy, energyRequest, combinedEnergyRequest     float64
	combinedRequestStructures, noneRequestStructures UnitSet
	massRequestStructures, energyRequestStructures   UnitSet
	model                                            *Simulator
	Data                                             EconomyData
}

func NewEconomy(model *Simulator) *Economy {
	return &Economy{
		model:                     model,
		combinedRequestStructures: make(UnitSet),
		noneRequestStructures:     make(UnitSet),
		massRequestStructures:     make(UnitSet),
		energyRequestStructures:   make(UnitSet),
	}
}

func (e *Economy) Reset() {
	e.Data.Mass = make([]util.Point, 0, 1024)
	e.Data.Energy = make([]util.Point, 0, 1024)
	e.Data.MassStorage = make([]util.Point, 0, 1024)
	e.Data.EnergyStorage = make([]util.Point, 0, 1024)
	e.Mass, e.Energy = e.model.Storage()
}

func (e *Economy) add(mass, energy float64) {
	e.Mass += mass
	e.Energy += energy
}

func (e *Economy) RequestCombined(unit Unit, mass, energy float64) {
	e.combinedMassRequest += mass
	e.combinedEnergyRequest += energy
	e.combinedRequestStructures.Add(unit)
}

func (e *Economy) RequestMass(unit Unit, mass float64) {
	e.massRequest += mass
	e.massRequestStructures.Add(unit)
}

func (e *Economy) RequestEnergy(unit Unit, energy float64) {
	e.energyRequest += energy
	e.energyRequestStructures.Add(unit)
}

func (e *Economy) RequestNone(unit Unit) {
	e.noneRequestStructures.Add(unit)
}

func (e *Economy) TotalRequestedResources() (mass, energy float64) {
	mass = e.massRequest + e.combinedMassRequest
	energy = e.energyRequest + e.combinedEnergyRequest
	return
}

func (e *Economy) StallRatio() (mass, energy float64) {
	mass = 1
	energy = 1
	if e.massRequest != 0 {
		mass = math.Min(1, e.Mass/e.massRequest)
	}
	if e.energyRequest != 0 {
		energy = math.Min(1, e.Energy/e.energyRequest)
	}
	return
}

func (e *Economy) CombinedStallRatio() (mass, energy, min float64) {
	mass = 1
	energy = 1
	massRequest, energyRequest := e.TotalRequestedResources()
	if massRequest != 0 {
		mass = math.Min(1, e.Mass/massRequest)
	}
	if energyRequest != 0 {
		energy = math.Min(1, e.Energy/energyRequest)
	}
	min = math.Min(mass, energy)
	return
}

func (e *Economy) Tick() {
	e.fetch()
	e.distribute()
	e.applyLimits()
	e.updateData()
}

func (e *Economy) applyLimits() {
	mass, energy := e.model.Storage()
	e.Mass = math.Min(e.Mass, mass)
	e.Energy = math.Min(e.Energy, energy)
}

func (e *Economy) fetch() {
	for unit := range e.model.Units {
		massYield := math.Max(0, unit.NetMassYield())
		energyYield := math.Max(0, unit.NetEnergyYield())
		e.add(massYield, energyYield)

		mass, energy := unit.RequestedResources()
		switch {
		case mass == 0 && energy == 0:
			e.RequestNone(unit)
		case mass == 0 && energy > 0:
			e.RequestEnergy(unit, energy)
		case mass > 0 && energy == 0:
			e.RequestMass(unit, mass)
		case mass > 0 && energy > 0:
			e.RequestCombined(unit, mass, energy)
		}
	}
}

func (e *Economy) distribute() {
	_, _, stallRatio := e.CombinedStallRatio()
	e.distributeCombined(stallRatio)
	mass, energy := e.StallRatio()
	e.distributeMass(mass)
	e.distributeEnergy(energy)
	e.distributeNone()
}

func (e *Economy) distributeCombined(ratio float64) {
	for unit := range e.combinedRequestStructures {
		mass, energy := unit.Update(ratio)
		e.Mass += mass
		e.Energy += energy
	}
	e.Mass -= e.combinedMassRequest * ratio
	e.Energy -= e.combinedEnergyRequest * ratio
	e.combinedMassRequest = 0
	e.combinedEnergyRequest = 0
	e.combinedRequestStructures = make(UnitSet)
}

func (e *Economy) distributeMass(ratio float64) {
	for unit := range e.massRequestStructures {
		mass, energy := unit.Update(ratio)
		e.Mass += mass
		e.Energy += energy
	}
	e.Mass -= e.massRequest * ratio
	e.massRequest = 0
	e.massRequestStructures = make(UnitSet)
}

func (e *Economy) distributeEnergy(ratio float64) {
	for unit := range e.energyRequestStructures {
		mass, energy := unit.Update(ratio)
		e.Mass += mass
		e.Energy += energy
	}
	e.Energy -= e.energyRequest * ratio
	e.energyRequest = 0
	e.energyRequestStructures = make(UnitSet)
}

func (e *Economy) distributeNone() {
	for unit := range e.noneRequestStructures {
		mass, energy := unit.Update(1)
		e.Mass += mass
		e.Energy += energy
	}
	e.noneRequestStructures = make(UnitSet)
}

func (e *Economy) updateData() {
	appendPointList(&e.Data.Mass, e.model.Time, e.Mass)
	appendPointList(&e.Data.Energy, e.model.Time, e.Energy)

	mass, energy := e.model.Storage()
	appendPointList(&e.Data.MassStorage, e.model.Time, mass)
	appendPointList(&e.Data.EnergyStorage, e.model.Time, energy)
}

func appendPointList(l *[]util.Point, x, y float64) {
	*l = append(*l, util.Point{X: x, Y: y})
}
