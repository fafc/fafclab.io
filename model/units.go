package model

func NewEnergyStorage(x, y float64) Unit {
	s := NewStructure("EnergyStorage", x, y).(*BaseStructure)
	s.color = "GoldenRod"
	s.massCost = 120
	s.energyCost = 2400
	s.energyStorage = 2000
	s.buildCost = 250
	s.icon = NewIcon("icon_structure_energy_rest")
	return s
}

func NewMassStorage(x, y float64) Unit {
	s := NewStructure("MassStorage", x, y).(*BaseStructure)
	s.color = "limegreen"
	s.massCost = 200
	s.massStorage = 500
	s.energyCost = 1500
	s.buildCost = 250
	s.icon = NewIcon("icon_structure_mass_rest")
	return s
}

type PowerGenerator struct {
	*BaseStructure
}

func NewPowerGenerator(name string, x, y float64) Unit {
	s := NewStructure(name, x, y).(*BaseStructure)
	s.color = "Gold"
	s.energyBonus = 0.5
	return &PowerGenerator{s}
}

func NewT1PowerGenerator(x, y float64) Unit {
	s := NewPowerGenerator("T1PowerGenerator", x, y).(*PowerGenerator)
	s.massCost = 75
	s.massYield = 0
	s.energyCost = 750
	s.energyYield = 20
	s.energyDiscount = 0.25
	s.buildCost = 125
	s.icon = NewIcon("icon_structure1_energy_rest")
	return s
}

func NewT2PowerGenerator(x, y float64) Unit {
	s := NewPowerGenerator("T2PowerGenerator", x, y).(*PowerGenerator)
	s.width = 3
	s.height = 3
	s.massCost = 1200
	s.massYield = 0
	s.energyCost = 12000
	s.energyYield = 500
	s.energyDiscount = 0.50
	s.buildCost = 2198
	s.icon = NewIcon("icon_structure2_energy_rest")
	return s
}

func NewT3PowerGenerator(x, y float64) Unit {
	s := NewPowerGenerator("T3PowerGenerator", x, y).(*PowerGenerator)
	s.width = 4
	s.height = 4
	s.massCost = 3240
	s.massYield = 0
	s.energyCost = 57600
	s.energyYield = 2500
	s.energyDiscount = 0.75
	s.buildCost = 6824
	s.icon = NewIcon("icon_structure3_energy_rest")
	return s
}

func NewT1HydrocarbonPowerGenerator(x, y float64) Unit {
	s := NewPowerGenerator("T1HydrocarbonPowerGenerator", x, y).(*PowerGenerator)
	s.width = 3
	s.height = 3
	s.massCost = 160
	s.massYield = 0
	s.energyCost = 800
	s.energyYield = 100
	s.energyDiscount = 0.50
	s.buildCost = 400
	s.icon = NewIcon("icon_structure1_energy_rest")
	return s
}

type MassProducer struct {
	*BaseStructure
}

func NewMassProducer(name string, x, y float64) Unit {
	s := NewStructure(name, x, y).(*BaseStructure)
	s.color = "LightGreen"
	s.massBonus = 0.5
	return &MassProducer{s}
}

func NewT1MassExtractor(x, y float64) Unit {
	s := NewMassProducer("T1MassExtractor", x, y).(*MassProducer)
	s.massCost = 36
	s.massYield = 2
	s.energyCost = 360
	s.energyYield = -2
	s.massDiscount = 0.075
	s.buildCost = 60
	s.buildRate = 10
	s.icon = NewIcon("icon_structure1_mass_rest")
	return s
}

func NewT2MassExtractor(x, y float64) Unit {
	s := NewMassProducer("T2MassExtractor", x, y).(*MassProducer)
	s.massCost = 900
	s.massYield = 6
	s.energyCost = 5400
	s.energyYield = -9
	s.massDiscount = 0.1
	s.buildCost = 900
	s.buildRate = 15
	s.icon = NewIcon("icon_structure2_mass_rest")
	return s
}

func NewT3MassExtractor(x, y float64) Unit {
	s := NewMassProducer("T3MassExtractor", x, y).(*MassProducer)
	s.massCost = 4600
	s.massYield = 18
	s.energyCost = 31625
	s.energyYield = -54
	s.massDiscount = 0.125
	s.buildCost = 2875
	s.icon = NewIcon("icon_structure3_mass_rest")
	return s
}

func NewT2MassFabricator(x, y float64) Unit {
	s := NewMassProducer("T2MassFabricator", x, y).(*MassProducer)
	s.color = "aquamarine"
	s.width = 1
	s.height = 1
	s.massCost = 200
	s.massYield = 1
	s.energyCost = 4000
	s.energyYield = -100
	s.massDiscount = 0.0125
	s.icon = NewIcon("icon_structure2_mass_rest")
	return s
}

func NewT3MassFabricator(x, y float64) Unit {
	s := NewMassProducer("T3MassFabricator", x, y).(*MassProducer)
	s.color = "aquamarine"
	s.width = 3
	s.height = 3
	s.massCost = 4000
	s.massYield = 16
	s.energyCost = 120000
	s.energyYield = -1500
	s.massDiscount = 0.2
	s.massBonus = 0.35
	s.icon = NewIcon("icon_structure3_mass_rest")
	return s
}

type ShieldGenerator interface {
	Structure
	ShieldRange() float64
}

type BaseShieldGenerator struct {
	*BaseStructure
	shieldRange float64
}

func (s *BaseShieldGenerator) ShieldRange() float64 {
	return s.shieldRange
}

func NewShieldGenerator(name string, x, y float64) Unit {
	s := NewStructure(name, x, y).(*BaseStructure)
	s.color = "SkyBlue"
	s.width = 3
	s.height = 3
	return &BaseShieldGenerator{BaseStructure: s}
}

func NewT2ShieldGenerator(name string, x, y float64) Unit {
	s := NewShieldGenerator(name, x, y).(*BaseShieldGenerator)
	s.icon = NewIcon("icon_structure2_shield_rest")
	return s
}

func NewT3ShieldGenerator(name string, x, y float64) Unit {
	s := NewShieldGenerator(name, x, y).(*BaseShieldGenerator)
	s.icon = NewIcon("icon_structure3_shield_rest")
	return s
}

func NewT2AeonShieldGenerator(x, y float64) Unit {
	s := NewT2ShieldGenerator("T2AeonShieldGenerator", x, y).(*BaseShieldGenerator)
	s.massCost = 480
	s.energyCost = 5760
	s.energyYield = -150
	s.shieldRange = 20
	s.buildCost = 950
	s.buildRate = 13.66
	return s
}

func NewT3AeonShieldGenerator(x, y float64) Unit {
	s := NewT3ShieldGenerator("T3AeonShieldGenerator", x, y).(*BaseShieldGenerator)
	s.massCost = 2400
	s.energyCost = 440000
	s.energyYield = -300
	s.shieldRange = 35
	s.buildCost = 4097
	return s
}

func NewT2UEFShieldGenerator(x, y float64) Unit {
	s := NewT2ShieldGenerator("T2UEFShieldGenerator", x, y).(*BaseShieldGenerator)
	s.massCost = 600
	s.energyCost = 6000
	s.energyYield = -200
	s.shieldRange = 26
	s.buildCost = 1150
	s.buildRate = 19.95
	return s
}

func NewT3UEFShieldGenerator(x, y float64) Unit {
	s := NewT3ShieldGenerator("T3UEFShieldGenerator", x, y).(*BaseShieldGenerator)
	s.massCost = 3300
	s.energyCost = 55000
	s.energyYield = -400
	s.shieldRange = 44
	s.buildCost = 4988
	return s
}

func NewT2CybranShieldGenerator1(x, y float64) Unit {
	s := NewT3ShieldGenerator("T2CybranShieldGenerator1", x, y).(*BaseShieldGenerator)
	s.massCost = 160
	s.energyCost = 2000
	s.energyYield = -100
	s.shieldRange = 18
	s.buildCost = 700
	s.buildRate = 19.2
	return s
}

func NewT2CybranShieldGenerator2(x, y float64) Unit {
	s := NewT3ShieldGenerator("T2CybranShieldGenerator2", x, y).(*BaseShieldGenerator)
	s.massCost = 460
	s.energyCost = 5000
	s.energyYield = -200
	s.shieldRange = 22
	s.buildCost = 775
	s.buildRate = 21
	return s
}

func NewT2CybranShieldGenerator3(x, y float64) Unit {
	s := NewT3ShieldGenerator("T2CybranShieldGenerator3", x, y).(*BaseShieldGenerator)
	s.massCost = 1260
	s.energyCost = 17000
	s.energyYield = -300
	s.shieldRange = 28
	s.buildCost = 2200
	s.buildRate = 29.3
	return s
}

func NewT3CybranShieldGenerator4(x, y float64) Unit {
	s := NewT3ShieldGenerator("T3CybranShieldGenerator4", x, y).(*BaseShieldGenerator)
	s.massCost = 2460
	s.energyCost = 41000
	s.energyYield = -400
	s.shieldRange = 34
	s.buildCost = 3515
	s.buildRate = 50.7
	return s
}

func NewT3CybranShieldGenerator5(x, y float64) Unit {
	s := NewT3ShieldGenerator("T3CybranShieldGenerator5", x, y).(*BaseShieldGenerator)
	s.massCost = 4260
	s.energyCost = 67666
	s.energyYield = -500
	s.shieldRange = 40
	s.buildCost = 7100
	return s
}

func NewT2SeraphimShieldGenerator(x, y float64) Unit {
	s := NewT3ShieldGenerator("T2SeraphimShieldGenerator", x, y).(*BaseShieldGenerator)
	s.massCost = 700
	s.energyCost = 7000
	s.energyYield = -250
	s.shieldRange = 28
	s.buildCost = 1250
	s.buildRate = 12.98
	return s
}

func NewT3SeraphimShieldGenerator(x, y float64) Unit {
	s := NewT3ShieldGenerator("T3SeraphimShieldGenerator", x, y).(*BaseShieldGenerator)
	s.massCost = 3600
	s.energyCost = 60000
	s.energyYield = -500
	s.shieldRange = 46
	s.buildCost = 5841
	return s
}

func NewACU(x, y float64) Unit {
	u := NewBuilder("ACU", x, y, NewOrderQueue()).(*BaseBuilder)
	u.icon = NewIcon("icon_commander_generic_rest")
	u.buildRate = 10
	u.massYield = 1
	u.massStorage = 650
	u.energyYield = 20
	u.energyStorage = 4000
	return u
}

func NewT1Engineer(x, y float64) Unit {
	u := NewBuilder("T1Engineer", x, y, NewOrderQueue()).(*BaseBuilder)
	u.icon = NewIcon("icon_land1_engineer_over")
	u.buildRate = 5
	u.buildCost = 260
	u.massCost = 52
	u.energyCost = 260
	return u
}

func NewT2Engineer(x, y float64) Unit {
	u := NewBuilder("T2Engineer", x, y, NewOrderQueue()).(*BaseBuilder)
	u.buildRate = 10
	u.buildCost = 800
	u.massCost = 160
	u.energyCost = 840
	return u
}

func NewT3Engineer(x, y float64) Unit {
	u := NewBuilder("T3Engineer", x, y, NewOrderQueue()).(*BaseBuilder)
	u.buildRate = 15
	u.buildCost = 2100
	u.massCost = 490
	u.energyCost = 3150
	return u
}
