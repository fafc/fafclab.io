package model

import (
	"reflect"
	"runtime"
	"strings"
)

type UnitConstructor func(x, y float64) Unit

var UnitDB = map[string]UnitConstructor{
	"es":    NewEnergyStorage,
	"ms":    NewMassStorage,
	"t1mx":  NewT1MassExtractor,
	"t2mx":  NewT2MassExtractor,
	"t3mx":  NewT3MassExtractor,
	"t1pg":  NewT1PowerGenerator,
	"t2pg":  NewT2PowerGenerator,
	"t3pg":  NewT3PowerGenerator,
	"t2mf":  NewT2MassFabricator,
	"t3mf":  NewT3MassFabricator,
	"t2ash": NewT2AeonShieldGenerator,
	"t3ash": NewT3AeonShieldGenerator,
	"t2ush": NewT2UEFShieldGenerator,
	"t3ush": NewT3UEFShieldGenerator,
	"t2cs1": NewT2CybranShieldGenerator1,
	"t2cs2": NewT2CybranShieldGenerator2,
	"t2cs3": NewT2CybranShieldGenerator3,
	"t3cs4": NewT3CybranShieldGenerator4,
	"t3cs5": NewT3CybranShieldGenerator5,
	"t2ssh": NewT2SeraphimShieldGenerator,
	"t3ssh": NewT3SeraphimShieldGenerator,
	"ACU":   NewACU,
	"t1eni": NewT1Engineer,
	"t2eni": NewT2Engineer,
	"t3eni": NewT3Engineer,
}

func GetUnitKey(name string) string {
	for k, v := range UnitDB {
		f := runtime.FuncForPC(reflect.ValueOf(v).Pointer()).Name()
		if strings.HasSuffix(f, name) {
			return k
		}
	}
	return ""
}
