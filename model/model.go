package model

import (
	"fmt"
	"gitlab.com/fafc/fafc.gitlab.io/util"
	"sort"
	"strconv"
	"strings"
)

// Model represents a structure that contains a collection of units and their corresponding economy
type Model struct {
	// Units is a UnitSet, which contains all the units that are part of the Model
	Units UnitSet

	// MassCost represents the sum of mass required to build all the units in the model
	MassCost float64

	// MassYield represents the sum of mass that the setup provides each tick
	MassYield float64

	// EnergyCost represents the sum of energy required to build all the units in the model
	EnergyCost float64

	// EnergyYield represents the sum of energy that the setup provides each tick
	EnergyYield float64
}

// NewModel returns an initialized Model.
func NewModel() *Model {
	return &Model{
		Units: make(UnitSet),
	}
}

// Load data from a map containing keys and units into the Model.
// The location should be formatted as `x,y`. The unit name can be found in `UnitDB`.
// This function is the inverse behavior of Export.
func (m *Model) Load(data map[string]string) {
	m.Units = make(UnitSet)
	for k, v := range data {
		xy := strings.SplitN(k, ",", 2)
		x, _ := strconv.ParseFloat(xy[0], 64)
		y, _ := strconv.ParseFloat(xy[1], 64)
		m.Units.Add(UnitDB[v](x, y))
	}
	m.recalculate()
}

// Export units from the Model in to a map containing keys and units.
// The key stores the location of a unit, formatted as `x,y`.
// The value stores the name of a unit, which can be found in `UnitDB`.
// This function is the inverse behavior of Load.
func (m *Model) Export() map[string]string {
	data := make(map[string]string, len(m.Units))
	for unit := range m.Units {
		data[unit.Location().String()] = GetUnitKey(unit.Name())
	}
	return data
}

// recalculate updates the MassCost, MassYield, EnergyCost and EnergyYield properties of the model.
func (m *Model) recalculate() {
	// Set all the values to
	m.MassCost = 0
	m.MassYield = 0
	m.EnergyCost = 0
	m.EnergyYield = 0

	for unit := range m.Units {
		if structure, ok := unit.(Structure); ok {
			structure.ClearAdjacency()
			for u := range m.Units {
				if unit == u {
					continue
				}
				if s, ok := u.(Structure); ok {
					structure.SetAdjacency(s)
				}
			}
		}

		m.MassCost += unit.MassCost()
		m.MassYield += unit.NetMassYield()
		m.EnergyCost += unit.EnergyCost()
		m.EnergyYield += unit.NetEnergyYield()
	}
}

// Add a Unit to Model. An error is returned when unit cannot be placed.
func (m *Model) Add(unit Unit) error {
	collisions := m.FindCollisions(unit)
	if len(collisions) != 0 {
		return fmt.Errorf("found %v conflicting units", len(collisions))
	}

	m.Units.Add(unit)
	m.recalculate()
	return nil
}

// DeleteUnits removes Units from the model.
func (m *Model) DeleteUnits(units ...Unit) {
	for _, unit := range units {
		m.Units.Delete(unit)
	}
}

// DeleteUnitSet removes all Units in a UnitSet from the Model.
func (m *Model) DeleteUnitSet(units UnitSet) {
	for unit := range units {
		m.Units.Delete(unit)
	}
}

// FindBetweenCoordinates returns a set of units that are inside of rectangle described by the given
// coordinates.
func (m *Model) FindBetweenCoordinates(pt1, pt2 *util.Point) UnitSet {
	// Return an empty UnitSet if points are empty.
	if pt1 == nil || pt2 == nil {
		return make(UnitSet)
	}

	start := pt1.Min(*pt2).Floor()
	size := pt2.Max(*pt1).Ceil().Subtract(start)

	if size.Magnitude() > 0 {
		return m.findIn(start, size)
	}

	set := make(UnitSet)
	set.Add(m.find(start.X, start.Y))
	return set
}

// FindCollisions checks if a given Unit overlaps with one of the units already in the Model
func (m *Model) FindCollisions(unit Unit) UnitSet {
	return m.findIn(unit.Location(), unit.Size())
}

// findIn returns a set of models that are contained within an area.
// start defines the top-left corner of the area.
// size defines the size of the area.
func (m *Model) findIn(start, size util.Point) UnitSet {
	units := make(UnitSet)
	for dx := 0.0; dx < size.X; dx++ {
		for dy := 0.0; dy < size.Y; dy++ {
			unit := m.find(start.X+dx, start.Y+dy)
			if unit != nil {
				units.Add(unit)
			}
		}
	}
	return units
}

// find a unit at a position `x, y` and return it if it exists.
func (m *Model) find(x, y float64) Unit {
	for unit := range m.Units {
		n, e, s, w := unit.Edges()
		if x >= w && x < e && y >= n && y < s {
			return unit
		}
	}
	return nil
}

// String returns the Model as a human-readable string.
func (m *Model) String() string {
	// Create a list of Unit string representations.
	units := make([]string, 0, len(m.Units))
	for unit := range m.Units {
		units = append(units, unit.String())
	}

	// Sort the list for consistent output
	sort.Strings(units)

	return fmt.Sprintf("MassCost: %v, MassYield: %v, EnergyCost: %v, EnergyYield: %v, Units: {%s}",
		m.MassCost, m.MassYield, m.EnergyCost, m.EnergyYield, strings.Join(units, ", "))
}
