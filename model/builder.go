package model

type Builder interface {
	Unit
	Active() bool
	AddOrder(order Order)
	RemoveOrder(order Order)
	Orders() *OrderQueue
	Reset()
	FetchCompletedUnit() Unit
}

type BaseBuilder struct {
	*BaseUnit
	massOverflow   float64
	energyOverflow float64
	queue          *OrderQueue
}

func NewBuilder(name string, x, y float64, buildQueue *OrderQueue) Builder {
	u := &BaseBuilder{
		BaseUnit: NewUnit(name, x, y).(*BaseUnit),
		queue:    buildQueue,
	}
	u.color = "red"
	return u
}

func (b *BaseBuilder) Active() bool {
	return b.queue.HasOrder()
}

func (b *BaseBuilder) AddOrder(order Order) {
	b.queue.Add(order)
}

func (b *BaseBuilder) RemoveOrder(order Order) {
	b.queue.Delete(order)
}

func (b *BaseBuilder) Orders() *OrderQueue {
	return b.queue
}

func (b *BaseBuilder) Reset() {
	b.queue.Clear()
}

func (b *BaseBuilder) NetMassYield() float64 {
	massOverflow := b.massOverflow
	b.massOverflow = 0
	return massOverflow + b.massYield
}

func (b *BaseBuilder) NetEnergyYield() float64 {
	energyOverflow := b.energyOverflow
	b.energyOverflow = 0
	return energyOverflow + b.energyYield
}

func (b *BaseBuilder) RequestedResources() (mass, energy float64) {
	return b.queue.RequestedResources(b)
}

func (b *BaseBuilder) Update(stallRatio float64) (massOverflow, energyOverflow float64) {
	m1, e1 := b.BaseUnit.Update(stallRatio)
	m2, e2 := b.queue.Update(b, stallRatio)
	return m1 + m2, e1 + e2
}

func (b *BaseBuilder) FetchCompletedUnit() Unit {
	return b.queue.FetchCompletedUnit()
}
