package model

type Icon struct {
	Name string
}

func NewIcon(name string) *Icon {
	return &Icon{name}
}
