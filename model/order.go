package model

type Order interface {
	ID() string
	Description() string
	Finished() bool
	Update(unit Unit, ratio float64) (mass, energy float64)
	RequestedResources(unit Unit) (mass, energy float64)
	Reset()
}
