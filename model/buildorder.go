package model

import (
	"fmt"
	"math"
)

type BuildOrder struct {
	Unit
	buildCostRemaining float64
	buildCostOverflow  float64
	finished           bool
}

func NewBuildOrder(unit Unit) Order {
	return &BuildOrder{
		Unit:               unit,
		buildCostRemaining: unit.BuildCost(),
	}
}

func (o *BuildOrder) ID() string {
	return fmt.Sprintf("buildorder-%s", o.Unit.ID())
}

func (o *BuildOrder) Description() string {
	return "build " + o.Name()
}

func (o *BuildOrder) Finished() bool {
	return o.finished
}

func (o *BuildOrder) Update(unit Unit, ratio float64) (mass, energy float64) {
	o.updateBuildProgress(ratio * unit.BuildRate())
	if o.finished {
		mass = o.MassCost() / o.BuildCost() * o.buildCostOverflow
		energy = o.EnergyCost() / o.BuildCost() * o.buildCostOverflow
		o.buildCostOverflow = 0
	}
	return
}

func (o *BuildOrder) RequestedResources(unit Unit) (mass, energy float64) {
	time := o.BuildCost() / unit.BuildRate()
	return o.MassCost() / time, o.EnergyCost() / time
}

func (o *BuildOrder) Reset() {
	o.finished = false
	o.buildCostRemaining = o.BuildCost()
	o.buildCostOverflow = 0
}

func (o *BuildOrder) updateBuildProgress(v float64) {
	o.buildCostRemaining -= v
	if o.buildCostRemaining <= 0 {
		o.buildCostOverflow += math.Abs(o.buildCostRemaining)
		o.buildCostRemaining = 0
		o.finished = true
	}
}

func (o *BuildOrder) FetchCompleted() Unit {
	if o.Finished() {
		return o.Unit
	}
	return nil
}
