package model

import (
	"fmt"
	"gitlab.com/fafc/fafc.gitlab.io/util"
	"math"
)

type Unit interface {
	// ID is an identifier for HTML-ids not for humans
	ID() string
	Name() string
	Color() string
	Icon() *Icon
	String() string

	Location() util.Point
	Center() util.Point
	Size() util.Point
	Edges() (n, e, s, w float64)
	Circumference() float64

	MassCost() float64
	MassYield() float64
	MassStorage() float64

	EnergyCost() float64
	EnergyYield() float64
	EnergyStorage() float64

	NetMassYield() float64
	NetEnergyYield() float64

	//Cost() (Mass, energy float64)
	//Yield() (Mass, energy float64)
	//NetYield() (Mass, energy float64)
	RequestedResources() (mass, energy float64)
	//Storage() (Mass, energy float64)

	StallRatio() float64
	Update(stallRatio float64) (massOverflow, energyOverflow float64)
	BuildCost() float64
	BuildRate() float64
}

type BaseUnit struct {
	name  string
	color string
	icon  *Icon

	x, y, width, height float64

	massCost, massYield, massStorage       float64
	energyCost, energyYield, energyStorage float64
	stallRatio, buildCost, buildRate       float64
}

func NewUnit(name string, x, y float64) Unit {
	return &BaseUnit{
		name:       name,
		color:      "white",
		x:          x,
		y:          y,
		width:      1,
		height:     1,
		stallRatio: 1,
		buildCost:  1,
	}
}

func (u *BaseUnit) ID() string {
	return fmt.Sprintf("unit-%T_%v_%v", *u, u.x, u.y)
}

func (u *BaseUnit) Name() string {
	return u.name
}

func (u *BaseUnit) Color() string {
	return u.color
}

func (u *BaseUnit) Icon() *Icon {
	return u.icon
}

func (u *BaseUnit) Location() util.Point {
	return util.Point{
		X: u.x,
		Y: u.y,
	}
}

func (u *BaseUnit) Center() util.Point {
	return util.Point{
		X: u.x + u.width/2,
		Y: u.y + u.height/2,
	}
}

func (u *BaseUnit) Size() util.Point {
	return util.Point{
		X: u.width,
		Y: u.height,
	}
}

func (u *BaseUnit) Edges() (n, e, s, w float64) {
	return u.y, u.x + u.width, u.y + u.height, u.x
}

func (u *BaseUnit) Circumference() float64 {
	return 2*u.width + 2*u.height
}

func (u *BaseUnit) MassCost() float64 {
	return u.massCost
}

func (u *BaseUnit) MassYield() float64 {
	return u.massYield
}

func (u *BaseUnit) MassStorage() float64 {
	return u.massStorage
}

func (u *BaseUnit) EnergyCost() float64 {
	return u.energyCost
}

func (u *BaseUnit) EnergyYield() float64 {
	return u.energyYield
}

func (u *BaseUnit) EnergyStorage() float64 {
	return u.energyStorage
}

func (u *BaseUnit) NetMassYield() float64 {
	return u.massYield * u.stallRatio
}

func (u *BaseUnit) NetEnergyYield() float64 {
	return u.energyYield * u.stallRatio
}

func (u *BaseUnit) RequestedResources() (mass, energy float64) {
	mass = math.Max(0, -u.NetMassYield())
	energy = math.Max(0, -u.NetEnergyYield())
	return
}

func (u *BaseUnit) StallRatio() float64 {
	return u.stallRatio
}

func (u *BaseUnit) Update(stallRatio float64) (massOverflow, energyOverflow float64) {
	u.stallRatio = stallRatio
	return
}

func (u *BaseUnit) BuildCost() float64 {
	return u.buildCost
}

func (u *BaseUnit) BuildRate() float64 {
	return u.buildRate
}

func (u *BaseUnit) String() string {
	return fmt.Sprintf("%s(%s)", u.Name(), u.Location())
}
