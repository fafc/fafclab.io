package model

// Simulator represents the state of a simulation.
type Simulator struct {
	*Model
	Time    float64
	Economy *Economy
	ACU     Builder
}

// NewSimulator returns a initialized Simulator.
func NewSimulator() *Simulator {
	s := &Simulator{
		Model: NewModel(),
	}
	s.Economy = NewEconomy(s)
	s.ClearSimulation()
	return s
}

// ClearSimulation removes all units and their orders from the simulation.
func (s *Simulator) ClearSimulation() {
	s.Units = make(UnitSet)
	s.ACU = UnitDB["ACU"](8, 8).(Builder)
	s.Add(s.ACU)
	s.initialize()
}

// initialize clears the economy state and resets the simulation time to zero.
func (s *Simulator) initialize() {
	s.Economy.Reset()
	s.Time = 0
}

// Run executes the simulation until all units have no orders left.
func (s *Simulator) Run() {
	s.reset()
	s.Units = make(UnitSet)
	s.Add(s.ACU)
	s.initialize()
	for s.active() {
		s.Tick()
	}
}

// Tick increments the simulation by a single tick.
func (s *Simulator) Tick() {
	s.Economy.Tick()
	s.FetchCompletedUnits()
	s.Time++
}

// active checks if units have uncompleted orders.
// Returns true if any unit is found with uncompleted orders.
func (s *Simulator) active() bool {
	for unit := range s.Units {
		if b, ok := unit.(Builder); ok && b.Active() {
			return true
		}
	}
	return false
}

// reset the simulation to the begin state but keep all the orders.
func (s *Simulator) reset() {
	for unit := range s.Units {
		if b, ok := unit.(Builder); ok {
			b.Reset()
		}
	}
}

// Storage calculates and returns the total mass and energy storage of the current units in the model.
func (s *Simulator) Storage() (mass, energy float64) {
	for unit := range s.Units {
		mass += unit.MassStorage()
		energy += unit.EnergyStorage()
	}
	return
}

// FetchCompleteUnits removes finished buildings and units from units that build them and adds them to the model.
func (s *Simulator) FetchCompletedUnits() {
	for unit := range s.Units {
		if b, ok := unit.(Builder); ok {
			if newUnit := b.FetchCompletedUnit(); newUnit != nil {
				s.Add(newUnit)
			}
		}
	}
}
