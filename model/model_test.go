package model

import (
	"fmt"
	"gitlab.com/fafc/fafc.gitlab.io/util"
	"testing"
)

// structureTestCase defines a struct that can be used in test cases.
type structureTestCase struct {
	// Units is a list with units that form a legal combination of structures and units.
	Units []Unit

	// Description is used to describe the setup of the units and help identify where a test failed.
	Description string

	// EnergyYield is the expected energy yield of these units.
	EnergyYield float64

	// MassYield is the expected mass yield of these units.
	MassYield float64
}

// AdjacencyTests is a list with multiple structureTestCases
var structureTests = []structureTestCase{
	{
		Description: "Single T1PowerGenerator without any adjacency",
		EnergyYield: 20,
		MassYield:   0,
		Units: []Unit{
			NewT1PowerGenerator(0, 0),
		},
	},
	{
		Description: "T1PowerGenerator with one adjacent E-Storage",
		EnergyYield: 22.5,
		MassYield:   0,
		Units: []Unit{
			NewT1PowerGenerator(0, 0),
			NewEnergyStorage(0, 1),
		},
	},
	{
		Description: "T1PowerGenerator with fully adjacent E-Storage",
		EnergyYield: 30,
		MassYield:   0,
		Units: []Unit{
			NewT1PowerGenerator(0, 0),
			NewEnergyStorage(0, 1),
			NewEnergyStorage(-1, 0),
			NewEnergyStorage(0, -1),
			NewEnergyStorage(1, 0),
		},
	},
	{
		Description: "T1MassExtractor with fully adjacent Energy(!)Storage",
		EnergyYield: -2,
		MassYield:   2,
		Units: []Unit{
			NewT1MassExtractor(0, 0),
			NewEnergyStorage(0, 1),
			NewEnergyStorage(-1, 0),
			NewEnergyStorage(0, -1),
			NewEnergyStorage(1, 0),
		},
	},
	{
		Description: "Single T1MassExtractor without any adjacency",
		EnergyYield: -2,
		MassYield:   2,
		Units: []Unit{
			NewT1MassExtractor(0, 0),
		},
	},
	{
		Description: "Single T1MassExtractor with one adjacent MassStorage",
		EnergyYield: -2,
		MassYield:   2.25,
		Units: []Unit{
			NewT1MassExtractor(0, 0),
			NewMassStorage(0, 1),
		},
	},
	{
		Description: "Single T2MassExtractor with fully adjacent MassStorage",
		EnergyYield: -9,
		MassYield:   9,
		Units: []Unit{
			NewT2MassExtractor(0, 0),
			NewMassStorage(0, 1),
			NewMassStorage(-1, 0),
			NewMassStorage(0, -1),
			NewMassStorage(1, 0),
		},
	},
	{
		Description: "Single T2MassExtractor with four MassStorage but not (!) adjacent",
		EnergyYield: -9,
		MassYield:   6,
		Units: []Unit{
			NewT2MassExtractor(0, 0),
			NewMassStorage(1, 1),
			NewMassStorage(-1, 1),
			NewMassStorage(-1, -1),
			NewMassStorage(1, -1),
		},
	},
	{
		Description: "Single T3MassExtractor with one adjacent T1PowerGenerator",
		EnergyYield: -30.625,
		MassYield:   18,
		Units: []Unit{
			NewT3MassExtractor(0, 0),
			NewT1PowerGenerator(0, 1),
		},
	},
	{
		Description: "T3PowerGenerator and T3MassFabricator adjacent",
		EnergyYield: 1281.25,
		MassYield:   16,
		Units: []Unit{
			NewT3PowerGenerator(0, 0),
			NewT3MassFabricator(4, 0),
		},
	},
	{
		Description: "T3PowerGenerator and T3MassFabricator not making full contact and thus no " +
			"bonus.",
		EnergyYield: 1000,
		MassYield:   16,
		Units: []Unit{
			NewT3PowerGenerator(0, 0),
			NewT3MassFabricator(4, 2),
		},
	},
	{
		Description: "ACU surrounded by MassStorage and EnergyStorage",
		EnergyYield: 20,
		MassYield:   1,
		Units: []Unit{
			NewACU(0, 0),
			NewMassStorage(0, 1),
			NewEnergyStorage(-1, 0),
			NewMassStorage(0, -1),
			NewEnergyStorage(1, 0),
		},
	},
}

// TestModel_recalculate tests the recalculate method of model.
func TestModel_recalculate(t *testing.T) {
	model := NewModel()
	for _, structureTest := range structureTests {
		// Clear Units from previous test
		model.Units = make(UnitSet)
		// Add units for current test
		for _, unit := range structureTest.Units {
			model.Add(unit)
		}
		// Check current test
		if model.EnergyYield != structureTest.EnergyYield ||
			model.MassYield != structureTest.MassYield {
			t.Errorf("Expected and calculated economy does not match for %q. \n"+
				"Mass exp: %f, cal: %f. Energy exp: %f, cal: %f.", structureTest.Description,
				structureTest.MassYield, model.MassYield, structureTest.EnergyYield,
				model.EnergyYield)
		}
	}
}

// collisionTestCase is used to describe test case that have overlapping building and should trigger
// collisions.
type collisionTestCase struct {
	// Units is a list with units that form a illegal combination of structures and units.
	Units []Unit

	// Description is to describe the setup of the units and help identify where a test failed.
	Description string
}

// collisionTests is a list with collisionTestCase structures.
var collisionTests = []collisionTestCase{
	{
		Description: "double overlapping T1PowerGenerator",
		Units: []Unit{
			NewT1PowerGenerator(0, 0),
			NewT1PowerGenerator(0, 0),
		},
	},
	{
		Description: "T1PowerGenerator and T2PowerGenerator overlapping",
		Units: []Unit{
			NewT1PowerGenerator(0, 0),
			NewT2PowerGenerator(-1, -1),
		},
	},
	{
		Description: "Builder and Structure overlapping",
		Units: []Unit{
			NewT1Engineer(0, 0),
			NewT1HydrocarbonPowerGenerator(-1, -2),
		},
	},
}

// TestModel_FindCollisions tests the FindCollisions method.
func TestModel_FindCollisions(t *testing.T) {
	model := NewModel()
	for _, collisionTest := range collisionTests {
		// Clear Units from previous test
		model.Units = make(UnitSet)
		// Create Error
		err := fmt.Errorf("")
		for _, unit := range collisionTest.Units {
			err = model.Add(unit)
			if err != nil {
				break
			}
		}
		if err == nil {
			t.Errorf(
				"Models are never allowed to overlap, "+
					"but was allowed in %q.", collisionTest.Description)
		}
	}
}

// TestModel_Export_Load tests the Load and the Export Methods of model.
func TestModel_Export_Load(t *testing.T) {
	model1 := NewModel()
	model2 := NewModel()
	for _, structureTest := range structureTests {
		// Clear Units from previous test
		model1.Units = make(UnitSet)
		model2.Units = make(UnitSet)
		// Add units for current test
		for _, unit := range structureTest.Units {
			model1.Add(unit)
		}
		data := model1.Export()
		model2.Load(data)
		err := compareModel(model1, model2)
		if err == nil {
			continue
		}
		t.Errorf("Models do not match because %s", err.Error())
	}
}

// compareModel returns an error message if the two models do not match.
func compareModel(model1 *Model, model2 *Model) error {
	if len(model1.Units) != len(model2.Units) {
		return fmt.Errorf("different unit count: expected %d units, got %d units",
			len(model1.Units), len(model2.Units))
	}
	for unit1 := range model1.Units {
		location1 := unit1.Location()
		unit2 := model2.find(location1.X, location1.Y)
		if unit2 == nil {
			return fmt.Errorf("second model does not have a matching %s at (%s)",
				unit1.Name(), location1)
		}
		if unit1.ID() != unit2.ID() {
			return fmt.Errorf("at location (%s) the models have different units",
				location1)
		}
	}
	return nil
}

// TestCompareModel tests the compareModel test function.
func TestCompareModel(t *testing.T) {
	model := NewModel()
	model.Add(NewT1MassExtractor(0, 0))
	model.Add(NewT1PowerGenerator(2, 0))
	// Compare a model with itself, should not error.
	if compareModel(model, model) != nil {
		t.Errorf("The compareModel function should return nil on two identical models, " +
			"but returned an error instead.")
	}
	model1 := NewModel()
	model1.Add(NewT1MassExtractor(1, 0))
	model1.Add(NewT1PowerGenerator(3, 0))
	// Compare models with same units at different locations, should error.
	if compareModel(model, model1) == nil {
		t.Errorf("Function compareModel should have returned an error when the units in " +
			"two models are not in the same position but didn't return an error.")
	}
	model2 := NewModel()
	model2.Add(NewT1MassExtractor(0, 0))
	// Compare models with different amount of units, should error.
	if compareModel(model, model2) == nil {
		t.Errorf("Function compareModel should return an error when the number of units " +
			"is not equal, but didn't return an error when two models with a different " +
			"amount of units was given")
	}
	model3 := NewModel()
	model.Add(NewT1MassExtractor(0, 0))
	model.Add(NewMassStorage(2, 0))
	// Compare models with different units that are at the same location, should error.
	if compareModel(model, model3) == nil {
		t.Errorf("Two models with different units at identical location should return " +
			"an error, but didn't.")
	}
}

// selectionTestCase defines a struct that can be used to test the selection tool
type selectionTestCase struct {
	Point1      util.Point
	Point2      util.Point
	Include     []Unit
	Exclude     []Unit
	Description string
}

// selectionTests instantiates some test cases.
var selectionsTests = []selectionTestCase{
	{
		Point1: util.Point{X: 1, Y: 1},
		Point2: util.Point{X: 1, Y: 1},
		Include: []Unit{
			NewT2AeonShieldGenerator(0, 0),
		},
		Exclude: []Unit{
			NewT3AeonShieldGenerator(-3, 0),
		},
		Description: "Click inside a unit.",
	},
	{
		Point1: util.Point{X: 0, Y: 0},
		Point2: util.Point{X: 6, Y: 6},
		Include: []Unit{
			NewT2UEFShieldGenerator(1, 1),
		},
		Exclude: []Unit{
			NewT3UEFShieldGenerator(7, 1),
		},
		Description: "Drag completely around a unit",
	},
	{
		Point1: util.Point{X: 0, Y: 0},
		Point2: util.Point{X: 0, Y: 0},
		Include: []Unit{
			NewT2CybranShieldGenerator1(0, 0),
		},
		Exclude: []Unit{
			NewT2CybranShieldGenerator2(-1, 5),
			NewT2CybranShieldGenerator3(6, 2),
			NewT3CybranShieldGenerator4(-3, -1),
			NewT3CybranShieldGenerator5(0, -3),
		},
		Description: "Click on the corner of a Unit",
	},
	{
		Point1: util.Point{X: 0, Y: 0},
		Point2: util.Point{X: 1, Y: 1},
		Include: []Unit{
			NewT1PowerGenerator(0, 0),
		},
		Exclude: []Unit{
			NewMassStorage(1, 1),
			NewMassStorage(-1, 1),
			NewMassStorage(-1, -1),
			NewMassStorage(1, -1),
			NewMassStorage(0, 1),
			NewMassStorage(-1, 0),
			NewMassStorage(0, -1),
			NewMassStorage(1, 0),
		},
		Description: "PG surrounded with storage, selection should only select PG in center.",
	},
	{
		Point1: util.Point{X: 0.5, Y: -2},
		Point2: util.Point{X: 0.5, Y: 2},
		Include: []Unit{
			NewT1PowerGenerator(0, 0),
			NewMassStorage(0, 1),
			NewMassStorage(0, -1),
		},
		Exclude: []Unit{
			NewMassStorage(1, 1),
			NewMassStorage(-1, 1),
			NewMassStorage(-1, -1),
			NewMassStorage(1, -1),
			NewMassStorage(-1, 0),
			NewMassStorage(1, 0),
		},
		Description: "PG surrounded with storage, selection should only select PG in center.",
	},
}

// unitSetMatch compares two UnitSet variables and returns True if they match.
func unitSetMatch(expected UnitSet, actual UnitSet) bool {
	if len(expected) != len(actual) {
		return false
	}
	matches := 0
	for unit1 := range expected {
		for unit2 := range actual {
			if unit1.ID() == unit2.ID() {
				matches++
				continue
			}
		}
	}
	return matches == len(expected)
}

// testFindBetweenCoordinates checks if the units in the model inside the selection box also
// match the set.
func testFindBetweenCoordinates(pt1, pt2 *util.Point, model *Model, set UnitSet) error {
	selectSet := model.FindBetweenCoordinates(pt1, pt2)
	if !unitSetMatch(set, selectSet) {
		return fmt.Errorf(
			"selecting from %s to %s did not return correct values\n"+
				"expected units:\n %s\n"+
				"actual units:\n %s",
			pt1, pt2, set, selectSet)
	}
	return nil
}

// TestModel_FindBetweenCoordinates will test the selection function of the model class.
func TestModel_FindBetweenCoordinates(t *testing.T) {
	modelCombined := NewModel()
	modelInclude := NewModel()
	for _, selectionTest := range selectionsTests {
		pt1 := selectionTest.Point1
		pt2 := selectionTest.Point2
		pt3 := util.Point{X: selectionTest.Point1.X, Y: selectionTest.Point2.Y}
		pt4 := util.Point{X: selectionTest.Point2.X, Y: selectionTest.Point1.Y}
		modelCombined.Units = make(UnitSet)
		modelInclude.Units = make(UnitSet)
		for _, unit := range selectionTest.Include {
			err := modelCombined.Add(unit)
			if err != nil {
				t.Error(err)
			}
			err = modelInclude.Add(unit)
			if err != nil {
				t.Error(err)
			}
		}
		for _, unit := range selectionTest.Exclude {
			err := modelCombined.Add(unit)
			if err != nil {
				t.Error(err)
			}
		}
		//Run 4 tests first from top-left to bottom-right
		err := testFindBetweenCoordinates(&pt1, &pt2, modelCombined, modelInclude.Units)
		if err != nil {
			t.Error(err)
		}
		// bottom-right to top-left
		err = testFindBetweenCoordinates(&pt2, &pt1, modelCombined, modelInclude.Units)
		if err != nil {
			t.Error(err)
		}
		// top-right to bottom-left
		err = testFindBetweenCoordinates(&pt3, &pt4, modelCombined, modelInclude.Units)
		if err != nil {
			t.Error(err)
		}
		// bottom-left to top-right
		err = testFindBetweenCoordinates(&pt4, &pt3, modelCombined, modelInclude.Units)
		if err != nil {
			t.Error(err)
		}
	}
	// Bonus Test to check all FindBetweenCoordinates handles nils correctly
	err := testFindBetweenCoordinates(nil, nil, modelCombined, make(UnitSet))
	if err != nil {
		t.Error(err)
	}
}

// TestModel_DeleteUnits will call the delete function of the model class and check if the
// correct set of unit is left.
func TestModel_DeleteUnits(t *testing.T) {
	modelCombined := NewModel()
	modelInclude := NewModel()
	for _, selectionTest := range selectionsTests {
		modelCombined.Units = make(UnitSet)
		modelInclude.Units = make(UnitSet)
		for _, unit := range selectionTest.Include {
			err := modelCombined.Add(unit)
			if err != nil {
				t.Error(err)
			}
			err = modelInclude.Add(unit)
			if err != nil {
				t.Error(err)
			}
		}
		for _, unit := range selectionTest.Exclude {
			err := modelCombined.Add(unit)
			if err != nil {
				t.Error(err)
			}
		}
		for _, unit := range selectionTest.Exclude {
			modelCombined.DeleteUnits(unit)
		}
		if !unitSetMatch(modelCombined.Units, modelInclude.Units) {
			t.Errorf("Units are not deleted correctly")
		}
	}
}

// TestModel_DeleteUnitSet test deletion of a set of units at once from the model.
func TestModel_DeleteUnitSet(t *testing.T) {
	modelCombined := NewModel()
	modelInclude := NewModel()
	modelExclude := NewModel()
	for _, selectionTest := range selectionsTests {
		modelCombined.Units = make(UnitSet)
		modelInclude.Units = make(UnitSet)
		modelExclude.Units = make(UnitSet)
		for _, unit := range selectionTest.Include {
			err := modelCombined.Add(unit)
			if err != nil {
				t.Error(err)
			}
			err = modelInclude.Add(unit)
			if err != nil {
				t.Error(err)
			}
		}
		for _, unit := range selectionTest.Exclude {
			err := modelCombined.Add(unit)
			if err != nil {
				t.Error(err)
			}
			err = modelExclude.Add(unit)
			if err != nil {
				t.Error(err)
			}
		}
		modelCombined.DeleteUnitSet(modelExclude.Units)
		if !unitSetMatch(modelCombined.Units, modelInclude.Units) {
			t.Errorf("Units are not deleted correctly")
		}
	}
}

func ExampleModel_Load() {
	model := NewModel()

	// Load a T3 mass extractor with adjacent storage
	model.Load(map[string]string{"15,14": "t3mx", "16,14": "ms", "15,13": "ms", "14,14": "ms", "15,15": "ms"})

	// Show the model
	fmt.Println(model)
	// Output:
	// MassCost: 5400, MassYield: 27, EnergyCost: 37625, EnergyYield: -54, Units: {MassStorage(14,14), MassStorage(15,13), MassStorage(15,15), MassStorage(16,14), T3MassExtractor(15,14)}
}
