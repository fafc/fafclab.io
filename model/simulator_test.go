package model

import (
	"testing"
)

// simulatorTestCase represents the container that contains the information needed to test the
// simulator.
type simulatorTestCase struct {
	Orders      [][]Order
	Description string
	MassYield   float64
	EnergyYield float64
}

// simulatorTestCases defines the test cases that can be used to test the simulator.
var simulatorTestCases = []simulatorTestCase{
	{
		Orders: [][]Order{
			{
				NewBuildOrder(NewT1PowerGenerator(2, 2)),
				NewBuildOrder(NewT1MassExtractor(0, 2)),
				NewBuildOrder(NewT1MassExtractor(5, 2)),
				NewBuildOrder(NewT1MassExtractor(2, -3)),
				NewBuildOrder(NewT1Engineer(8, 15)),
			},
			{
				NewBuildOrder(NewT1HydrocarbonPowerGenerator(10, 8)),
			},
		},
		Description: "Build a simple startup, note that the ACU builds a T1 Engineer",
		MassYield:   7,
		EnergyYield: 134,
	},
	{
		Orders: [][]Order{
			{
				NewBuildOrder(NewT3PowerGenerator(12, 2)),
				NewBuildOrder(NewT3PowerGenerator(16, 2)),
				NewBuildOrder(NewT3PowerGenerator(20, 2)),
				NewBuildOrder(NewT3PowerGenerator(12, 6)),
				NewBuildOrder(NewT3PowerGenerator(16, 6)),
				NewBuildOrder(NewT3PowerGenerator(20, 6)),
				NewBuildOrder(NewT3PowerGenerator(12, 10)),
				NewBuildOrder(NewT3PowerGenerator(16, 10)),
				NewBuildOrder(NewT3PowerGenerator(20, 10)),
				NewBuildOrder(NewT3PowerGenerator(12, 14)),
				NewBuildOrder(NewT3PowerGenerator(16, 14)),
				NewBuildOrder(NewT3PowerGenerator(20, 14)),
			},
		},
		Description: "Build twelve T3 Pgens to generate very long sim-time from stall",
		MassYield:   1,
		EnergyYield: 30020,
	},
	{
		Orders: [][]Order{
			{
				NewBuildOrder(NewT1PowerGenerator(1, 0)),
				NewBuildOrder(NewT1PowerGenerator(-1, 0)),
				NewBuildOrder(NewT1PowerGenerator(0, 1)),
				NewBuildOrder(NewT1PowerGenerator(0, -1)),
				NewBuildOrder(NewEnergyStorage(0, 0)),
				NewBuildOrder(NewT1MassExtractor(5, 0)),
				NewBuildOrder(NewT1MassExtractor(6, 1)),
				NewBuildOrder(NewT1Engineer(2, 2)),
				NewBuildOrder(NewT1MassExtractor(7, 2)),
				NewBuildOrder(NewT1MassExtractor(7, 0)),
				NewBuildOrder(NewT2Engineer(10, 10)),
			},
			{
				NewBuildOrder(NewMassStorage(5, 1)),
				NewBuildOrder(NewMassStorage(6, 0)),
				NewBuildOrder(NewMassStorage(6, 2)),
				NewBuildOrder(NewMassStorage(7, 1)),
				NewBuildOrder(NewT1HydrocarbonPowerGenerator(-5, -5)),
			},
			{
				NewBuildOrder(NewT2MassExtractor(2, -2)),
				NewBuildOrder(NewT2PowerGenerator(-6, -1)),
			},
		},
		Description: "Build twelve T3 Pgens to generate very long sim-time from stall",
		MassYield:   11.5,
		EnergyYield: 202,
	},
}

// includeOrder initializes the orders for the initial builder as well as the orders for any
// builders that will be constructed later.
func includeOrder(builder Builder, orders [][]Order, recursion int) {
	// return, if the recursion is bigger than the number of order lists.
	if recursion > len(orders) {
		return
	}
	for _, order := range orders[recursion] {
		builder.AddOrder(order)
		buildOrder, ok := order.(*BuildOrder)
		// continue to add the next order, if the order is not a BuildOrder.
		if !ok {
			continue
		}
		newBuilder, ok := buildOrder.Unit.(Builder)
		// continue to add the next order, if the order is not a Builder
		if !ok {
			continue
		}
		// The newest order we added will build another unit with building capabilities.
		// We call this function again but on the builder unit that is in the recently added order.
		includeOrder(newBuilder, orders, recursion+1)
	}
}

// simulatorInit initializes and return a simulator object according to the given test case.
func simulatorInit(testCase simulatorTestCase) *Simulator {
	simulator := NewSimulator()
	includeOrder(simulator.ACU, testCase.Orders, 0)
	return simulator
}

// TestSimulator simulates the test cases and check if the end result is correct.
func TestSimulator(t *testing.T) {
	for _, testCase := range simulatorTestCases {
		simulator := simulatorInit(testCase)
		simulator.Run()
		simulator.recalculate()
		if simulator.MassYield != testCase.MassYield {
			t.Errorf("Expected MassYield %f, got MassYield %f", testCase.MassYield, simulator.MassYield)
		}
		if simulator.EnergyYield != testCase.EnergyYield {
			t.Errorf("Expected EnergyYield %f, got EnergyYield %f", testCase.EnergyYield, simulator.EnergyYield)
		}
	}
}

// benchmarkSimulator will run a benchmark on a specific test case that is indicated by `i`.
func benchmarkSimulator(i int) {
	simulator := simulatorInit(simulatorTestCases[i])
	simulator.Run()
	simulator.recalculate()
}

// BenchmarkNewSimulator0 will benchmark the first test case.
func BenchmarkNewSimulator0(b *testing.B) {
	benchmarkSimulator(0)
}

// BenchmarkNewSimulator1 will benchmark the second test case.
func BenchmarkNewSimulator1(b *testing.B) {
	benchmarkSimulator(1)
}
