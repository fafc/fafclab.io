package model

type Structure interface {
	Unit
	MassDiscount() float64   // Mass consumption discount fraction per adjacent unit.
	MassBonus() float64      // Mass production bonus fraction when fully surrounded.
	EnergyDiscount() float64 // Energy consumption discount fraction when fully surrounded.
	EnergyBonus() float64    // Energy production bonus fraction when fully surrounded.

	SetAdjacency(Structure)
	ClearAdjacency()
}

type BaseStructure struct {
	*BaseUnit
	massDiscount, massBonus     float64
	energyDiscount, energyBonus float64
	adjacent                    UnitSet
}

func NewStructure(name string, x, y float64) Structure {
	return &BaseStructure{
		BaseUnit: NewUnit(name, x, y).(*BaseUnit),
		adjacent: make(UnitSet),
	}
}

func (b *BaseStructure) NetMassYield() float64 {
	if b.massYield > 0 {
		return b.stallRatio * b.massProducerYield()
	}
	return b.stallRatio * b.massConsumerYield()
}

func (b *BaseStructure) massProducerYield() float64 {
	y := b.massYield
	for unit := range b.adjacent {
		u, ok := unit.(Structure)
		if ok && u.MassStorage() > 0 {
			y += b.adjacentPercent(u) * b.MassBonus() * b.massYield
		}
	}
	return y
}

func (b *BaseStructure) massConsumerYield() float64 {
	y := b.massYield
	for unit := range b.adjacent {
		u, ok := unit.(Structure)
		if ok && u.MassYield() > 0 {
			y -= b.adjacentPercent(u) * u.MassDiscount() * b.massYield
		}
	}
	return y
}

func (b *BaseStructure) NetEnergyYield() float64 {
	if b.energyYield > 0 {
		return b.stallRatio * b.energyProducerYield()
	}
	return b.stallRatio * b.energyConsumerYield()
}

func (b *BaseStructure) energyProducerYield() float64 {
	y := b.energyYield
	for unit := range b.adjacent {
		u, ok := unit.(Structure)
		if ok && u.EnergyStorage() > 0 {
			y += b.adjacentPercent(u) * b.EnergyBonus() * b.energyYield
		}
	}
	return y
}

func (b *BaseStructure) energyConsumerYield() float64 {
	y := b.energyYield
	for unit := range b.adjacent {
		u, ok := unit.(Structure)
		if ok && u.EnergyYield() > 0 {
			y -= b.adjacentPercent(u) * u.EnergyDiscount() * b.energyYield
		}
	}
	return y
}

func (b *BaseStructure) MassDiscount() float64 {
	return b.massDiscount
}

func (b *BaseStructure) MassBonus() float64 {
	return b.massBonus
}

func (b *BaseStructure) EnergyDiscount() float64 {
	return b.energyDiscount
}

func (b *BaseStructure) EnergyBonus() float64 {
	return b.energyBonus
}

func (b *BaseStructure) SetAdjacency(structure Structure) {
	b.adjacent.Add(structure)
}

func (b *BaseStructure) ClearAdjacency() {
	b.adjacent = make(UnitSet)
}

func (b *BaseStructure) adjacentPercent(structure Structure) float64 {
	return b.adjacentTiles(structure) / b.Circumference()
}

func (b *BaseStructure) adjacentTiles(structure Structure) float64 {
	n := 0
	e := 1
	s := 2
	w := 3

	ns := b.adj(structure, n, s, e, w)
	if ns > 0 {
		return ns
	}

	return b.adj(structure, e, w, s, n)
}

func (b *BaseStructure) adj(structure Structure, n, s, e, w int) float64 {
	bEdges := edgeArray(b.Edges())
	sEdges := edgeArray(structure.Edges())

	// Check if the structure is adjacent on north or south edge
	if bEdges[n] == sEdges[s] || bEdges[s] == sEdges[n] {
		// Check if we are smaller or equal to structure width: we have full adjacency to it
		if bEdges[w] >= sEdges[w] && bEdges[e] <= sEdges[e] {
			return bEdges[e] - bEdges[w]
		}
		// Check if the structure is smaller or equal to us: it has full adjacency to us
		if sEdges[w] >= bEdges[w] && sEdges[e] <= bEdges[e] {
			return sEdges[e] - sEdges[w]
		}
	}
	return 0
}

func edgeArray(n, e, s, w float64) [4]float64 {
	return [4]float64{n, e, s, w}
}
