GO=go
GOROOT=$(shell go env GOROOT)
EXEC=$(GOROOT)/misc/wasm/go_js_wasm_exec

export GOOS=js
export GOARCH=wasm

all: public/powerflower.wasm public/simulator.wasm public/wasm_exec.js

test:
	$(GO) test -cover -v -exec="$(EXEC)" ./...

public/wasm_exec.js: $(GOROOT)/misc/wasm/wasm_exec.js
	cp $< $@

clean:
	rm public/*.wasm public/wasm_exec.js

public/powerflower.wasm:
	$(GO) build -o $@ ./cmd/$(basename $(notdir $@))

public/simulator.wasm:
	$(GO) build -o $@ ./cmd/$(basename $(notdir $@))

.PHONY: public/powerflower.wasm public/simulator.wasm test clean
